//
//  AppDelegate.swift
//  KuTalk
//
//  Created by Fu on 1/26/21.
//

import UIKit
import Firebase
import IQKeyboardManager

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        AppAppearance.config()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
        setupNotification(application)
//        setupTheme()
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {(granted, error) in
             // Use granted to check if permission is granted
          }
        handleRootScreen()
        subscribeTopic()
        return true
    }
    
    func subscribeTopic() {
        Messaging.messaging().subscribe(toTopic: "Room1") { (error) in
            
        }
    }
    
    //did click banner notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("notificationnnnn")
        print(response.notification.request.content.userInfo)
        
        
//        if let dataString = response.notification.request.content.userInfo["data"] as? String {
//            if let dataUTF8 = dataString.data(using: .utf8) {
//                if let dict = try? JSONSerialization.jsonObject(with: dataUTF8, options: []) as? [String: Any] {
////                    handleDataReceiveFromNotification(dataNoti: dict)
//                }
//            }
//        }
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        print(deviceToken.hexString)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if Auth.auth().canHandle(url) {
          return true
        }
        return false
    }
}

//MARK: - Helper

extension AppDelegate {
    private func handleRootScreen() {
        if AppDataSingleton.shared.currentUser != nil {
            if let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController() {
                window?.rootViewController = vc
                window?.makeKeyAndVisible()
            }
        } else {
            if let vc = UIStoryboard.init(name: "Authentication", bundle: nil).instantiateInitialViewController() {
                window?.rootViewController = vc
                window?.makeKeyAndVisible()
            }
        }
    }
    
    private func setupTheme() {
        AppDataSingleton.shared.updateInterfaceStyle()
    }
    
    private func setupNotification(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }
//
      // Print full message.
      print("hello")
        print(userInfo)

      completionHandler(UIBackgroundFetchResult.newData)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
       // If you are receiving a notification message while your app is in the background,
       // this callback will not be fired till the user taps on the notification launching the application.
       // TODO: Handle data of notification
       // With swizzling disabled you must let Messaging know about the message, for Analytics
       // Messaging.messaging().appDidReceiveMessage(userInfo)
       // Print message ID.
       if let messageID = userInfo[gcmMessageIDKey] {
         print("Message ID: \(messageID)")
       }

        print("hi")
       // Print full message.
       print(userInfo)
     }
}

//MARK: - MessagingDelegate

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
//        Messaging.messaging().token { token, error in
//            if let error = error {
//                print("Error fetching FCM registration token: \(error)")
//            } else if let token = token {
////                print("FCM registration token: \(token)")
//                if let currentUser = AppDataSingleton.shared.currentUser,
//                   token != currentUser.pushNotificationId {
//                    currentUser.updateNotificationID(with: token)
//                }
//            }
//        }
        print("Firebase registration token: \(String(describing: fcmToken))")
        
        AppDataSingleton.shared.pushNotificationID = fcmToken
        
        if let currentUser = AppDataSingleton.shared.currentUser,
           let fcmToken = fcmToken { // fcmToken != currentUser.pushNotificationId 
            currentUser.pushNotificationId = fcmToken
            AppDataSingleton.shared.currentUser = currentUser
            currentUser.updateNotificationID(with: fcmToken)
        }

        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
}
