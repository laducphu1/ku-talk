//
//  MainTabbarVC.swift
//  KuTalk
//
//  Created by mac on 1/26/21.
//

import UIKit

class MainTabbarVC: UITabBarController {
    var lastTabbarItem = UITabBarItem()
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        
    }

    //MARK: - Private
    private func setupData () {
        tabBar.layer.applySketchShadow(color: UIColor.init(white: 0, alpha: 0.1), alpha: 1.0, x: 0, y: -4, blur: 40, spread: 0)
        tabBar.clipsToBounds = false
        tabBar.layer.masksToBounds = false
    }
}
