//
//  RoomsListVC.swift
//  KuTalk
//
//  Created by flydino on 2/5/21.
//

import UIKit

class RoomsListVC: UIViewController {
    
    @IBOutlet var views: [UIView]!
    @IBOutlet weak var usersNumberNorthLabel: UILabel!
    @IBOutlet weak var usersNumberSouthLabel: UILabel!
    @IBOutlet weak var usersNumberCentralLabel: UILabel!
    
    private var userIdListRoom1 = [String]()
    private var userIdListRoom2 = [String]()
    private var userIdListRoom3 = [String]()
    
    var isBlock = true
    
    //MARK: - View LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    //MARK: - Helper

    private func configureUI() {
        self.navigationItem.title = isBlock ? "Danh sách bị khóa" : "Danh sách cấm chát"
        for view in views {
            view.layer.cornerRadius = 12
            view.addShadow(ofColor: .gray, radius: 3, offset: .zero, opacity: 0.5)
        }
        
        for roomID in ["RoomId1", "RoomId2", "RoomId3"] {
            if isBlock {
                Room.getUsersBlockOfRoom(roomID) { [weak self] (userIdList) in
                    guard let self = self else { return }
                    switch roomID {
                    case "RoomId1":
                        self.userIdListRoom1 = userIdList
                        self.usersNumberNorthLabel.text = "\(userIdList.count) thành viên"
                        break
                    case "RoomId2":
                        self.userIdListRoom2 = userIdList
                        self.usersNumberCentralLabel.text = "\(userIdList.count) thành viên"
                        break
                    default:
                        self.userIdListRoom3 = userIdList
                        self.usersNumberSouthLabel.text = "\(userIdList.count) thành viên"
                        break
                    }
                }
            } else {
                Room.getUsersMuteOfRoom(roomID) { [weak self] (userIdList) in
                    guard let self = self else { return }
                    switch roomID {
                    case "RoomId1":
                        self.userIdListRoom1 = userIdList
                        self.usersNumberNorthLabel.text = "\(userIdList.count) thành viên"
                        break
                    case "RoomId2":
                        self.userIdListRoom2 = userIdList
                        self.usersNumberCentralLabel.text = "\(userIdList.count) thành viên"
                        break
                    default:
                        self.userIdListRoom3 = userIdList
                        self.usersNumberSouthLabel.text = "\(userIdList.count) thành viên"
                        break
                    }
                }
            }
        }
    }
    
    private func showBlockListVC(index: Int) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "BlockListVC") as? BlockListVC {
            switch index {
            case 0:
                vc.blockList = userIdListRoom1
                break
            case 1:
                vc.blockList = userIdListRoom2
                break
            default:
                vc.blockList = userIdListRoom3
            }
            vc.index = index
            vc.isBlock = isBlock
            self.navigationController?.pushViewController(vc)
        }
    }
    
    //MARK: - Action
    
    @IBAction func group1DidClick(_ sender: Any) {
        showBlockListVC(index: 0)
    }
    
    @IBAction func group2DidClick(_ sender: Any) {
        showBlockListVC(index: 1)
    }
    
    @IBAction func group3DidClick(_ sender: Any) {
        showBlockListVC(index: 2)
    }
}
