//
//  InformationVC.swift
//  KuTalk
//
//  Created by Lê Phước on 2/4/21.
//

import Foundation
import UIKit
import SVProgressHUD

class InformationVC: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    
    var userID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserInfo()
    }
    
    private func getUserInfo() {
        guard let userID = userID else {
            return
        }
        SVProgressHUD.show()
        User().getUserInfo(userID) { [weak self] (user) in
            SVProgressHUD.dismiss()
            guard let self = self else {return}
            if let user = user {
                self.userNameLabel.text = user.userName
                self.phoneNumberLabel.text = user.phone
                if let url = URL(string: user.avatar) {
                    self.avatarImageView.sd_setImage(with: url, completed: nil)
                } else {
                    self.avatarImageView.image = UIImage(named: "img_default_avatar")
                }
            }

        }
    }
    
    @IBAction func didClickCallButton(_ sender: Any) {
        if let phone = userID {
            UIApplication.shared.makeCall(to: phone)
        }
    }
}
