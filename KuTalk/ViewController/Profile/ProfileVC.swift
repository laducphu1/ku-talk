//
//  ProfileVC.swift
//  KuTalk
//
//  Created by mac on 1/26/21.
//

import UIKit
import FirebaseAuth
import MobileCoreServices
import SDWebImage
import SVProgressHUD

class ProfileVC: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var darkModeView: UIView!
    @IBOutlet weak var blockListView: UIView!
    @IBOutlet weak var muteListView: UIView!
    
    @IBOutlet weak var avatarContainerView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var editAvatarButton: UIButton!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var adminPhoneLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!

    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var switchButton: UISwitch!
    
    private var infoShadow: UIView?
    
    var userName: String = "" {
        didSet {
            userNameLabel.text = userName
        }
    }
    
    // MARK: - Helper
    private func configUI() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapAvatar))
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.addGestureRecognizer(tapGesture)
        
        let tapNameGesture = UITapGestureRecognizer(target: self, action: #selector(editUserNameAction(_:)))
        userNameLabel.isUserInteractionEnabled = true
        userNameLabel.addGestureRecognizer(tapNameGesture)
        
        infoView.generateOuterShadow(shadowColor: UIColor.appColor(.appShadowColor), offSet: .zero, opacity: 0.3, shadowRadius: 15, cornerRadius: 10)
        
        switchButton.isOn = AppDataSingleton.shared.isDarkModeEnable
        
        if let user = AppDataSingleton.shared.currentUser {
            blockListView.isHidden = !user.isAdmin
            muteListView.isHidden = !user.isAdmin
            if let url = URL(string: user.avatar) {
                avatarImageView.sd_setImage(with: url, completed: nil)
            }
            userNameLabel.text = user.userName
            phoneLabel.text = user.phone
        }
        
        if #available(iOS 13.0, *) {
            darkModeView.isHidden = false
        } else {
            darkModeView.isHidden = true
        }
    }
    
    private func showInputAlert(title: String, message: String, label: UILabel) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        //1. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = label.text
        }
        
        // 2. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Đồng ý", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            if let text = textField?.text, text.isEmpty == false {
                label.text = text
                self.userName = text
                self.updateUserName(text)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: nil))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openGallary() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func showAlertPickImage() {
        let alert = UIAlertController(title: "Chọn avatar", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Thư viện ảnh", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Hủy", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
       if #available(iOS 13.0, *) {
           if (traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection)) {
            let color = UIColor.appColor(.whiteBackground).cgColor
            logOutButton.layer.borderColor = color
           }
       }
    }
    
    //MARK: - Action
    @IBAction private func didTapAvatar() {
        showAlertPickImage()
    }
    
    @IBAction func editUserNameAction(_ sender: UIButton) {
        showInputAlert(title: "Tên hiển thị", message: "Vui lòng nhập tên người dùng mới của bạn", label: userNameLabel)
    }
    
    @IBAction func logOutAction(_ sender: UIButton) {
        do {
            try Auth.auth().signOut()
            AppDataSingleton.shared.currentUser = nil
            AppDataSingleton.shared.setInitialVC(isHome: false)
        } catch let signOutError as NSError {
          print ("Error signing out: %@", signOutError)
        }
    }
    
    @IBAction func callSupportAction(_ sender: UIButton) {
        let supportNumber = "+84569911969"
        UIApplication.shared.makeCall(to: supportNumber)
    }
    
    @IBAction func didClickDarkModeSwitch(_ sender: Any) {
        AppDataSingleton.shared.isDarkModeEnable = !AppDataSingleton.shared.isDarkModeEnable
        AppDataSingleton.shared.updateInterfaceStyle()
    }
    
    @IBAction func didClickPolicy(_ sender: Any) {
        if let path = Bundle.main.path(forResource: "policy", ofType: ".pdf") {
            let docs = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            docs.name = "Chính sách và nội quy"
            docs.delegate = self
            docs.presentPreview(animated: true)
        }
    }
    
    @IBAction func didClickSupport(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SupportVC") {
            self.navigationController?.pushViewController(vc)
        }
    }
    
    @IBAction func didClickBlockList(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RoomsListVC") {
            self.navigationController?.pushViewController(vc)
        }
    }
    
    @IBAction func didClickMuteList(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RoomsListVC") as? RoomsListVC {
            vc.isBlock = false
            self.navigationController?.pushViewController(vc)
        }
    }
    
    //MARK: - Firebase
    private func updateAvatar(_ image: UIImage) {
        if let user = AppDataSingleton.shared.currentUser {
            SVProgressHUD.show()
            user.uploadImage(image: image) { (url) in
                SVProgressHUD.dismiss()
                if url == nil {
                    DispatchQueue.main.async {
                        self.showAlert(title: "Thông báo", message: "Xảy ra lỗi khi cập nhật avatar!")
                    }
                }
            }
        }
    }
    
    private func updateUserName(_ userName: String) {
        if let user = AppDataSingleton.shared.currentUser {
            SVProgressHUD.show()
            user.updateUserName(userName) { (error) in
                SVProgressHUD.dismiss()
                if error != nil {
                    DispatchQueue.main.async {
                        self.showAlert(title: "Thông báo", message: "Xảy ra lỗi khi cập nhật tên tài khoản!")
                    }
                }
            }
        }
    }
}

//MARK: - Lifecycle
extension ProfileVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
}

//MARK: - ImagePickerDelegate
extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage {
            avatarImageView.image = image
            updateAvatar(image)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK: - UIDocumentInteractionControllerDelegate
extension ProfileVC: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
}
