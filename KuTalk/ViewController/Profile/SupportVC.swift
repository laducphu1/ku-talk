//
//  SupportVC.swift
//  KuTalk
//
//  Created by Lê Phước on 1/27/21.
//

import UIKit
import SwifterSwift
import SVProgressHUD

class SupportVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var QAList = [QA]()
    
    // MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        getQAList()
    }
    
    // MARK: - Helper
    private func configUI() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        
        self.navigationItem.title = "Trợ giúp"
    }
    
    //MARK: - Firebase
    private func getQAList() {
        SVProgressHUD.show()
        QA.shared.getAllQA { [weak self] (list) in
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            if let list = list {
                self.QAList = list
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
}

// MARK: - TableView
extension SupportVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return QAList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: QACell.self)
        cell.setup(with: QAList[indexPath.row], index: indexPath.row)
        return cell
    }
}

// MARK: - Cell
class QACell: UITableViewCell {
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    
    static let id = "QACell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(with qa: QA, index: Int) {
        questionLabel.text = "\(index + 1). " + qa.question
        answerLabel.text = "Trả lời: " + qa.answer
    }
}
