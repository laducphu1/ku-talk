//
//  BlockListVC.swift
//  KuTalk
//
//  Created by Lê Phước on 2/5/21.
//

import UIKit
import SVProgressHUD

class BlockListVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var blockList = [String]()
    var index = 0
    var roomID = ""
    var isBlock = true
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
    
    //MARK: - Helper
    private func configUI() {
        switch index {
        case 0:
            self.navigationItem.title = "Khu vực miền Bắc"
            self.roomID = "RoomId1"
            break
        case 1:
            self.navigationItem.title = "Khu vực miền Trung"
            self.roomID = "RoomId2"
            break
        default:
            self.navigationItem.title = "Khu vực miền Nam"
            self.roomID = "RoomId3"
            break
        }
    }
    
    private func showAlertWarning(userID: String) {
        let message = isBlock ? "Bạn có muốn tắt block thành viên này?" : "Bạn có muốn mở chat thành viên này?"
        let alert = UIAlertController(title: "Thông báo", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Không", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Có", style: .default, handler: { [weak self] (action) in
            guard let self = self else { return }
            SVProgressHUD.show()
            if self.isBlock {
                self.unBlockMember(userID)
            } else {
                self.unLockChatMember(userID)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func unLockChatMember(_ userID: String) {
        User.shared.removeMuteFromRoom(self.roomID, userID: userID) { (error) in
            if error == nil {
                Room.removeUserFromMuteOfRoom(self.roomID, userID: userID) { (error) in
                    SVProgressHUD.dismiss()
                    if error != nil {
                        
                    }
                }
            } else {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    private func unBlockMember(_ userID: String) {
        User.shared.removeBlockFromRoom(self.roomID, userID: userID) { (error) in
            if error == nil {
                Room.removeUserFromBlocksOfRoom(self.roomID, userID: userID) { (error) in
                    SVProgressHUD.dismiss()
                    if error != nil {
                        
                    }
                }
            } else {
                SVProgressHUD.dismiss()
            }
        }
    }
}

//MARK: - TableView
extension BlockListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blockList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: BlockMemberCell.self, for: indexPath)
        cell.setupCell(with: blockList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showAlertWarning(userID: blockList[indexPath.row])
    }
}

//MARK: - BlockMemberCell
class BlockMemberCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(with phone: String) {
        phoneLabel.text = phone
        
        User.shared.getUserInfo(phone) { [weak self] (user) in
            guard let self = self else { return }
            if let user = user {
                self.nameLabel.text = user.userName
                if let url = URL(string: user.avatar) {
                    self.avatarImageView.sd_setImage(with: url, completed: nil)
                }
            }
        }
    }
}
