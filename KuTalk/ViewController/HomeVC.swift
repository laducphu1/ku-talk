//
//  HomeVC.swift
//  KuTalk
//
//  Created by mac on 1/26/21.
//

import UIKit

class HomeVC: UIViewController {
    
    @IBOutlet var views: [UIView]!
    @IBOutlet weak var usersNumberNorthLabel: UILabel!
    @IBOutlet weak var usersNumberSouthLabel: UILabel!
    @IBOutlet weak var usersNumberCentralLabel: UILabel!
    @IBOutlet weak var numberConversationsLabel: UILabel!
    @IBOutlet weak var supportTitleCenterConstrain: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        User.obseverUser { (user) in

        }
        if let user = AppDataSingleton.shared.currentUser, !user.isAdmin {
            User.fetchAdminFcmIdsList { (ids, error) in
                if let ids = ids {
                    AppDataSingleton.shared.fcmTokens = ids
                }
            }
//            user.observeBlockRooms()
        }
    }
    
    private func configureUI() {
        for view in views {
            view.layer.cornerRadius = 12
            view.addShadow(ofColor: .gray, radius: 3, offset: .zero, opacity: 0.5)
        }
        AppDataSingleton.shared.updateInterfaceStyle()
    }
    
    private func showChatVC(_ id: String, name: String) {
        if let user = AppDataSingleton.shared.currentUser, user.blocks.contains(id) {
            Utils.showAlertController(title: "Thông báo", message: "Bạn bị cấm vào phòng này", nil)
            return
        }
        let chatVC = storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        chatVC.groupId = id
        chatVC.groupName = name
        navigationController?.pushViewController(chatVC, animated: true)
        
    }
    
    @IBAction func group1DidClick(_ sender: Any) {
        showChatVC("RoomId1", name: "Khu vực miền Bắc")
    }
    
    @IBAction func group2DidClick(_ sender: Any) {
        showChatVC("RoomId2", name: "Khu vực miền Trung")
    }
    
    @IBAction func group3DidClick(_ sender: Any) {
        showChatVC("RoomId3", name: "Khu vực miền Nam")
    }
    
    @IBAction func supportDidClick(_ sender: Any) {
        guard let currentUser = AppDataSingleton.shared.currentUser else {
            return
        }
        if let vc = self.storyboard?.instantiateViewController(withClass: SupportConversationsVC.self),
           currentUser.isAdmin {
            self.navigationController?.pushViewController(vc)
        } else {
            showChatVC("SupportRooms/\(currentUser.phone)", name: "Hỗ trợ")
        }
    }
    
    //MARK: - Firebase
    
    private func getNumberConversations() {
        Conversation.shared.getConversations { [weak self] (count) in
            guard let self = self else { return }
            self.numberConversationsLabel.text = "Số cuộc hội thoại: \(count)"
        }
    }
}
