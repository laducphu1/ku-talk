//
//  SupportConversationsVC.swift
//  KuTalk
//
//  Created by Lê Phước on 2/1/21.
//

import UIKit
import SVProgressHUD

class SupportConversationsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var conversations = [Conversation]()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getConversations()
    }
    
    //MARK: - Helper
    private func setupUI() {
        self.navigationItem.title = "Hỗ trợ"
    }
    
    //MARK: - Firebase
    private func getConversations() {
        SVProgressHUD.show()
        Conversation.shared.getConversations { [weak self] (conversations, error) in
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            if let conversations = conversations {
                self.conversations = conversations
                self.tableView.reloadData()
            } else {
                self.showAlert(message: error?.localizedDescription)
            }
        }
    }
}

//MARK: - TableView
extension SupportConversationsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: SPConversationCell.self)
        cell.setupCell(with: conversations[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let conver = conversations[indexPath.row]
        let chatVC = storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        chatVC.groupId = "SupportRooms/\(conver.roomId)"
        chatVC.groupName = "Hỗ trợ"
        chatVC.pushNotificationId = conver.pushNotificationId
        navigationController?.pushViewController(chatVC, animated: true)

    }
}

//MARK: - UITableViewCell
class SPConversationCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var timeMessageLabel: UILabel!

    //MARK: - View LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: - Public
    func setupCell(with conversation: Conversation) {
        getUserInfo(with: conversation.roomId)
        if conversation.lastMessageTime != -0.0 {
            timeMessageLabel.text = AppDataSingleton.shared.handleTime(with: Date(timeIntervalSince1970: conversation.lastMessageTime/1000))
        }
        lastMessageLabel.text = conversation.lastMessage
    }
    
    //MARK: - Private
    
    //MARK: - Firebase
    private func getUserInfo(with phone: String) {
        User.shared.getUserInfo(phone) { [weak self] (user) in
            guard let self = self else { return }
            if let user = user {
                if let url = URL(string: user.avatar) {
                    self.avatarImageView.sd_setImage(with: url, completed: nil)
                }
                self.nameLabel.text = user.userName
            }
        }
    }
}
