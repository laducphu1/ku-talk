//
//  ChatVC.swift
//  KuTalk
//
//  Created by Fu on 1/26/21.
//

import UIKit
import AsyncDisplayKit
import SVProgressHUD
import IQKeyboardManager
import ISEmojiView

class ChatVC: UIViewController {
    
    @IBOutlet weak var pinnedView: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    var groupId: String = ""
    var groupName: String = ""
    var pushNotificationId: String?
    private var items = [Message]()
    @IBOutlet weak var nodeContainerView: UIView!
    private var tableNode: ASTableNode!
    @IBOutlet weak var pinMessageLabel: UILabel!
    @IBOutlet weak var inputTextView: EmojiTextField!
    var pinnedMessage: Message?
    @IBOutlet weak var chatInputView: UIView!
    private var ids = [String]()
    private let placeholderString = "Nhập tin nhắn..."
    private let keyboardSettings = KeyboardSettings(bottomType: .categories)
    private var emojiView: EmojiView!
    private var isEmojiKeyboard = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchData()
        fetchPinnedMessage()
        inputTextView.languageCode = "vi-VN"
        if let user = AppDataSingleton.shared.currentUser, user.mutes.contains(groupId) {
            chatInputView.isHidden = true
        } else {
            chatInputView.isHidden = false
        }
        User.obseverUser { [weak self] (user) in
            guard let self = self else {return}
            if let user = user {
                if user.mutes.contains(self.groupId) {
                    self.chatInputView.isHidden = true
                    SVProgressHUD.showInfo(withStatus: "Bạn đã bị cấm chat")
                    SVProgressHUD.dismiss(withDelay: 1.5)
                }
                if user.blocks.contains(self.groupId) {
                    SVProgressHUD.showInfo(withStatus: "Bạn đã kick ra khỏi phòng")
                    SVProgressHUD.dismiss(withDelay: 1.5)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        fetchMutedUser()
        removedMessage()
        registrationIds()

        emojiView = EmojiView(keyboardSettings: keyboardSettings)
        emojiView.translatesAutoresizingMaskIntoConstraints = false
        emojiView.delegate = self
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = groupName
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    private func textInputMode() -> UITextInputMode? {
        for mode in UITextInputMode.activeInputModes {
            if mode.primaryLanguage != "emoji" {
                return mode
            }
        }
        return nil
    }
    
    private func registrationIds() {
        Room.registrationIds(roomId: groupId) { [weak self] (ids, _) in
            self?.ids = ids
        }
    }
    
    private func blockUser(userId: String) {
        guard let user = AppDataSingleton.shared.currentUser else {return}
        user.blockUser(id: groupId, userId: userId)
    }
    
    private func muteUser(userId: String) {
        guard let user = AppDataSingleton.shared.currentUser else {return}
        user.muteUser(id: groupId, userId: userId)

    }
    
    private func composeMessage(type: MessageType, content: String)  {
        guard let user = AppDataSingleton.shared.currentUser else {
            return
        }
        let message = Message()
        message.type = type.rawValue
        message.content = content
        message.userId = user.phone
        message.userName = user.userName
        message.timestamp = String(Date().timeIntervalSince1970)
        message.avatar = user.avatar
        message.isAdmin = user.isAdmin
        message.send(toID: groupId) { [weak self] (status) in
            if status {
                if let self = self {
                    if self.groupId.contains("SupportRooms")  {
                        if user.isAdmin {
                            if let pushNotiId = self.pushNotificationId {
                                PushNotification.shared.sendNotification(message: message, ids: [pushNotiId], groupName: "Hỗ trợ")
                            }
                        } else {
                            Conversation.updateConversationFrom(message: message)
                            PushNotification.shared.sendNotification(message: message, ids: AppDataSingleton.shared.fcmTokens, groupName: "Hỗ trợ")
                        }
                    } else {
                        PushNotification.shared.sendNotification(message: message, ids: self.ids, groupName: self.groupName)
                    }
                }
                
            }
        }
        
    }
    private func pinMessage(message: Message) {
        message.pindMessage(toID: groupId) { (_) in
            
        }
    }
    
    private func delete(message: Message) {
        message.delete(toID: groupId) { (_) in
            
        }
    }
    
    private func edit(message: Message) {
        message.update(to: groupId) { [weak self] (error) in
            guard let self = self else { return }
            if error == nil {
                var index = -1
                index = self.items.firstIndex(where: { (msg) -> Bool in
                    msg.messageId == message.messageId
                }) ?? -1
                self.items[index] = message
                self.tableNode.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
            }
        }
    }
    
    private func configureUI() {

        let user = AppDataSingleton.shared.currentUser ?? User()
        let padding:CGFloat = user.isAdmin ? 80 : 50
        inputTextView.textContainerInset = UIEdgeInsets(top: 15, left: 5, bottom: 15, right: padding)

        inputTextView.text = placeholderString
        inputTextView.textColor = UIColor.appColor(.blackTextColor).withAlphaComponent(0.8)
        
        nodeContainerView.backgroundColor = .clear
        tableNode = ASTableNode.init()
        nodeContainerView.addSubview(tableNode.view)
        tableNode.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableNode.view.topAnchor.constraint(equalTo: nodeContainerView.topAnchor),
            tableNode.view.leadingAnchor.constraint(equalTo: nodeContainerView.leadingAnchor),
            tableNode.view.trailingAnchor.constraint(equalTo: nodeContainerView.trailingAnchor),
            tableNode.view.bottomAnchor.constraint(equalTo: nodeContainerView.bottomAnchor),
        ])
        
        //        tableNode.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: nodeContainerView.frame.size.height)
        tableNode.view.tableFooterView = UIView.init()
        tableNode.view.backgroundColor = UIColor(hex: "F4F5F6")
        tableNode.delegate = self
        tableNode.dataSource = self
        tableNode.view.separatorStyle = .none
        tableNode.view.rowHeight = UITableView.automaticDimension
        tableNode.backgroundColor = UIColor.appColor(.tableViewBackground)         
        var contentInset = tableNode.contentInset
        contentInset.bottom = 20
        contentInset.top = 60
        tableNode.contentInset = contentInset
        tableNode.view.showsVerticalScrollIndicator = false
        if let user = AppDataSingleton.shared.currentUser {
            cameraButton.isHidden = !user.isAdmin
            inputTextView.textContainerInset = UIEdgeInsets(top: 15, left: 5, bottom: 15, right: user.isAdmin ? 80 : 50)
        }
    }
    
    private func fetchData() {
        Message.fetchAllMessages(roomId: groupId) { [weak self] (data, error) in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                self.items.append(data)
                self.tableNode.insertRows(at: [IndexPath(row: self.items.count - 1, section: 0)], with: .fade)
                self.tableNode.scrollToRow(at: IndexPath(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
            }
          
            
        }
    }
    
    private func removedMessage() {
        Message.removedMessage(roomId: groupId) { [weak self] (data, error) in
            guard let self = self else {return}
            if let index = self.items.firstIndex(where: {$0.messageId == data.messageId}) {
                DispatchQueue.main.async {
                    self.items.remove(at: index)
                    self.tableNode.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                }
            }
        }

    }
    
    private func fetchMutedUser() {
        guard let user = AppDataSingleton.shared.currentUser else {return}
        Message.muteList(roomId: groupId) { [weak self] (userId, _) in
            if userId == user.phone {
                self?.chatInputView.isHidden = true

            }
        }
    }
    
    private func fetchPinnedMessage() {
        Message.fetchPinnedMessage(roomId: groupId) { [weak self] (data, error) in
            guard let self = self else {return}
            if let data = data {
                self.pinnedMessage = data
                self.pinnedView.isHidden = false
                if data.type == "photo" {
                    self.pinMessageLabel.text = "Photo"
                } else {
                    self.pinMessageLabel.text = data.content
                }
            } else {
                self.pinnedView.isHidden = true
            }
        }
    }
    
    private func showInformationVC(userID: String) {
        if let vc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "InformationVC") as? InformationVC {
            vc.userID = userID
            self.navigationController?.pushViewController(vc)
        }
    }
    
    private func showInputAlert(message: Message) {
        let alert = UIAlertController(title: "Sửa tin nhắn", message: "", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.text = message.content
        }
        
        alert.addAction(UIAlertAction(title: "Đồng ý", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            if let text = textField?.text, text.isEmpty == false {
                message.content = text
                self.edit(message: message)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Actions
    
    @IBAction func clearPinAction(_ sender: Any) {
        self.pinnedView.isHidden = true
        Message.removePinMessage(roomId: groupId) { (_, error) in
            if let error = error {
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                SVProgressHUD.dismiss(withDelay: 1.0)
            }
        }
    }
    
    @IBAction func cameraAction(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        if let text = self.inputTextView.text,
           text != placeholderString,
           text.trim() != "",
           text.replacingOccurrences(of: "\n", with: "") != "" {
            if text.count > 0 {
                if let currentUser = AppDataSingleton.shared.currentUser,
                   currentUser.isAdmin == false {
                    let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                    let matches = detector.matches(in: text, options: [], range: NSRange(location: 0, length: text.utf16.count))
                    if matches.count > 0 {
                        SVProgressHUD.showError(withStatus: "Bạn không thể gửi link trong nhóm chat này!")
                        SVProgressHUD.dismiss(withDelay: 1.0)
                        return
                    }
                }
                self.composeMessage(type: .text, content: text.trimmingCharacters(in: .whitespacesAndNewlines))
                self.inputTextView.text = ""
            }
        }
    }
    
    @IBAction func selectPinViewAction(_ sender: Any) {
        guard let pinMessage = pinnedMessage else {return}
        if let index = items.firstIndex(where: {$0.messageId == pinMessage.messageId}) {
            self.tableNode.scrollToRow(at: IndexPath(row: index, section: 0), at: .middle, animated: true)
        }
    }
    @IBAction func emojiKeyboard(_ sender: Any) {
        isEmojiKeyboard = !isEmojiKeyboard
        inputTextView.inputView = isEmojiKeyboard ? emojiView : nil
        inputTextView.reloadInputViews()
    }
    
    private func didSelectCellAtIndexPath(_ message: Message) {
        guard let user = AppDataSingleton.shared.currentUser, user.isAdmin else {return}
        let alert = UIAlertController(title: "Lựa chọn", message: "", preferredStyle: .actionSheet)

        if message.isAdmin {
            alert.addAction(UIAlertAction(title: "Ghim tin nhắn", style: .default, handler: { (_) in
                self.pinMessage(message: message)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Xoá tin nhắn", style: .default, handler: { (_) in
            self.delete(message: message)
        }))
        
        if user.phone == message.userId, message.type != "photo" {
            alert.addAction(UIAlertAction(title: "Sửa tin nhắn", style: .default, handler: { (_) in
                self.showInputAlert(message: message)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Huỷ", style: .destructive, handler: { (_) in
            
        }))
        present(alert, animated: true, completion: nil)
    }
    
    private func didSelectCellAtIndexPathMember(_ message: Message) {
        let alert = UIAlertController(title: "Lựa chọn", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Sửa tin nhắn", style: .default, handler: { (_) in
            self.showInputAlert(message: message)
        }))
        
        alert.addAction(UIAlertAction(title: "Huỷ", style: .destructive, handler: { (_) in
            
        }))
        present(alert, animated: true, completion: nil)
    }
    
    private func handleClickMessage(_ message: Message) {
            if let currentUser = AppDataSingleton.shared.currentUser {
                if currentUser.isAdmin == true {
                    didSelectCellAtIndexPath(message)
                } else {
                    if currentUser.phone == message.userId, message.type != "photo" {
                        didSelectCellAtIndexPathMember(message)
                    }
                }
            }
    }
}

//MARK: - ASTableDelegate + ASTableDatasource

extension ChatVC: ASTableDelegate, ASTableDataSource {
    
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: ASTableView, nodeForRowAt indexPath: IndexPath) -> ASCellNode {
        let message = self.items[indexPath.row]
        if message.userId == AppDataSingleton.shared.currentUser!.phone {
            let node = SenderMessageNode.init(message: message)
            node.selectionStyle = .none
            node.didTapMessageContent = { [weak self] in
                self?.handleClickMessage(message)
            }
            return node
        }
        let node = ReceiverMessageNode.init(message: message)
        node.didTapMessageContent = { [weak self] in
            self?.handleClickMessage(message)
        }
        node.didSelectAvatarImage = { [weak self] in
            guard let self = self else {
                return
            }
            guard let currentUser = AppDataSingleton.shared.currentUser,
                  currentUser.isAdmin == true else {
                return
            }
            let alert = UIAlertController(title: "Lựa chọn", message: "", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Cấm chat", style: .default, handler: {[weak self] (_) in
                self?.muteUser(userId: message.userId)
            }))
            alert.addAction(UIAlertAction(title: "Kick ra khỏi phòng", style: .default, handler: { [weak self](_) in
                self?.blockUser(userId: message.userId)
                
            }))
            alert.addAction(UIAlertAction(title: "Xem thông tin", style: .default, handler: { [weak self](_) in
                self?.showInformationVC(userID: message.userId)
            }))
            alert.addAction(UIAlertAction(title: "Huỷ", style: .destructive, handler: { (_) in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
        node.selectionStyle = .none
        return node
    }
}


extension ChatVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeholderString {
            textView.textColor = UIColor.appColor(.blackTextColor)
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.count == 0 {
            textView.text = placeholderString
            textView.textColor = UIColor.appColor(.blackTextColor).withAlphaComponent(0.8)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
//        textView.sizeToFit()
        print(nodeContainerView.frame)
        print(tableNode.frame)
        tableNode.performBatchUpdates(nil, completion: nil)
        tableNode.scrollToRow(at: IndexPath(row: items.count - 1, section: 0), at: .bottom, animated: true)
        //        scrollViewHeightConstraint.constant = fixedHeight + collectionViewHeightConstraint.constant + textViewHeightConstraint.constant
    }
}
extension ChatVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            image.uploadImage { (urlString, error) in
                if !urlString.isEmpty {
                    self.composeMessage(type: .photo, content: urlString)
                    
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension ChatVC: EmojiViewDelegate {
    // callback when tap a emoji on keyboard
    func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
        inputTextView.insertText(emoji)
    }

    // callback when tap change keyboard button on keyboard
    func emojiViewDidPressChangeKeyboardButton(_ emojiView: EmojiView) {
    }
        
    // callback when tap delete button on keyboard
    func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        inputTextView.deleteBackward()
    }

    // callback when tap dismiss button on keyboard
    func emojiViewDidPressDismissKeyboardButton(_ emojiView: EmojiView) {
        inputTextView.resignFirstResponder()
    }
}
