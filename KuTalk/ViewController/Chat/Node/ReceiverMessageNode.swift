//
//  ReceiverMessageNode.swift
//  KuTalk
//
//  Created by Fu on 1/26/21.
//

import UIKit
import AsyncDisplayKit
import twitter_text

class ReceiverMessageNode: ASCellNode {

    var message = Message()
    let width = UIScreen.main.bounds.size.width - 100
    public var didSelectAvatarImage: (() -> (Void))?
    var didTapMessageContent:(() -> Void)?

    //MARK: - initial
    
    init(message: Message) {
        super.init()
        self.message = message
        self.automaticallyManagesSubnodes = true
        style.minHeight = ASDimensionMake(45)
        backgroundColor = .clear
        getUserInfo()
    }
    
    //MARK: - Private
    
    private func getUserInfo() {
        User().getUserInfo(message.userId) { [weak self] (user) in
            guard let self = self else {return}
            if let user = user {
                let name = user.isAdmin ? "Admin": user.userName
                self.senderNameTextNode.attributedText = NSAttributedString(string: name, attributes: ReceiverMessageNode.nameAttributes)
                if let url = URL(string: user.avatar) {
                    self.avatarImageNode.url = url
                } else {
                    self.avatarImageNode.image = UIImage(named: "img_default_avatar")
                }
            }

        }
    }
    
    private lazy var senderNameTextNode = { () -> ASTextNode in
        let node = ASTextNode()
        let name = message.isAdmin ? "Admin": message.userName
        node.attributedText = NSAttributedString(string: name, attributes: ReceiverMessageNode.nameAttributes)
        return node
    }()
    
    private lazy var messageImageNode = { () -> ASNetworkImageNode in
        let node = ASNetworkImageNode.init()
        node.style.preferredSize = CGSize.init(width: 260, height: 300)

        node.setURL(URL.init(string: self.message.content), resetToDefault: true)
        node.defaultImage = UIImage(named: "img_default_avatar")
        node.cornerRadius = 15
        node.addTarget(self, action: #selector(didTapMessage), forControlEvents: .touchUpInside)

        return node
    }()
    
    private lazy var backgroundMessageImage = { () -> ASImageNode in
        let node = ASImageNode.init()
        node.image = UIImageView.imageWithImage(image: UIImage.init(named: "bubble_received")!)
        node.style.minHeight = ASDimensionMake(34)
        node.style.minWidth = ASDimensionMake(34)
        
        return node
    }()
    
    
    private lazy var avatarImageNode = { () -> ASNetworkImageNode in
        let node = ASNetworkImageNode.init()
        node.style.preferredSize = CGSize.init(width: 30, height: 30)
        node.cornerRadius = 15
//        if let url = URL(string: message.avatar) {
//            node.url = url
//        } else {
//            node.image = #imageLiteral(resourceName: "img_avatar")
//        }
        node.addTarget(self, action: #selector(didClickAvatarImage), forControlEvents: .touchUpInside)
        return node
    }()
    
    @objc private func didClickAvatarImage() {
        if let user = AppDataSingleton.shared.currentUser, user.phone == message.userId {
            return
        }
        didSelectAvatarImage?()
    }
    
    private lazy var messageTextNode = { () -> ASTextNode in
        let node = ASTextNode.init()
        let height = 1000
        let width = self.width
//        node.style.maxSize = CGSize.init(width: width, height: 1000)
        node.style.maxHeight = ASDimensionMake(1000)
        node.style.maxWidth = ASDimensionMake(UIScreen.main.bounds.size.width - 100)
        node.style.flexShrink = 1
        node.style.alignSelf = .start
        node.delegate = self
        node.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        if message.content.isSingleEmoji {
            node.backgroundColor = .clear
            node.attributedText = NSAttributedString.init(string: self.message.content, attributes: ReceiverMessageNode.emojiAttributes)

        } else {
            node.attributedText = NSAttributedString.init(string: self.message.content, attributes: ReceiverMessageNode.textAttributes)
            node.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            
            node.backgroundColor = UIColor.appColor(.messageReceiver)
            let text: NSString = NSString(string: message.content)
            let updateString = NSMutableAttributedString(string: message.content, attributes: ReceiverMessageNode.textAttributes)
            let entities = TwitterText.entities(inText: text as String)
            var tag: String = ""
            var tags: [String] = [String]()
            
            for entity:TwitterTextEntity in entities  {
                
                let r: NSRange = entity.range
                
                if entity.type == TwitterTextEntityType.URL {
                    tag = text.substring(with: r)
                    print(tag)
                    let url = URL(string: tag)
                    if url != nil {
                        var mutableActiveLinkAttributes: [AnyHashable : Any] = [:]
                        mutableActiveLinkAttributes[NSAttributedString.Key.foregroundColor] = UIColor(hex: "0044CC")
                        
                        if let url = URL(string: url?.absoluteString ?? "") {
                            mutableActiveLinkAttributes[tag] = url
                            tags.append(tag)
                        }
                        
                        if let mutableActiveLinkAttributes = mutableActiveLinkAttributes as? [NSAttributedString.Key : Any] {
                            updateString.addAttributes(mutableActiveLinkAttributes, range: r)
                        }
                    }
                }
            }
            if tags.count > 0 {
                node.linkAttributeNames = tags
                node.isUserInteractionEnabled = true
            } else {
                node.isUserInteractionEnabled = false
            }
            node.attributedText = updateString
        }
        node.clipsToBounds = true
        node.cornerRadius = 4
        
        node.addTarget(self, action: #selector(didTapMessage), forControlEvents: .touchUpInside)

        return node
    }()
    override func layout() {
        super.layout()
        messageTextNode.cornerRadius = 6
    }
    
    @objc private func didTapMessage() {
        didTapMessageContent?()
    }
}
//MARK: ReceiverMessageNode Extention

extension ReceiverMessageNode {
    

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {

        let contentChild = message.type == "photo" ? messageImageNode: messageTextNode

        let contentLayout = ASStackLayoutSpec(direction: .vertical,
                                              spacing: 4,
                                              justifyContent: .start,
                                              alignItems: .start,
                                              children: [senderNameTextNode, contentChild])
        messageImageNode.style.preferredSize = CGSize.init(width: 260, height: 300)
       let stackLayout = ASStackLayoutSpec.init(direction: .horizontal,
                                             spacing: 5,
                                             justifyContent: .start,
                                             alignItems: .end,
                                             children: [avatarImageNode, contentLayout])
        return ASInsetLayoutSpec.init(insets: UIEdgeInsets(top: 0, left: 16, bottom: 16, right: 0), child: stackLayout)
        //        return stackLayout
    }
    
    static var textAttributes: [NSAttributedString.Key: Any] {
        return [NSAttributedString.Key.foregroundColor: UIColor.appColor(.receiverText),
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
    }
    
    static var sendTextAttributes: [NSAttributedString.Key: Any] {
        return [NSAttributedString.Key.foregroundColor: UIColor.appColor(.sendText),
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
    }
    
    static var emojiAttributes: [NSAttributedString.Key: Any] {
        return [NSAttributedString.Key.foregroundColor: UIColor.appColor(.whiteBackground),
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 25)]
    }
    
    static var nameAttributes: [NSAttributedString.Key: Any] {
        return [NSAttributedString.Key.foregroundColor: UIColor.appColor(.sendText),
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)]
    }
}
extension ReceiverMessageNode: ASTextNodeDelegate {
    func textNode(_ textNode: ASTextNode!, tappedLinkAttribute attribute: String!, value: Any!, at point: CGPoint, textRange: NSRange) {
        if let tag = attribute{
            if let url = URL(string: tag) {
                UIApplication.shared.open(url)
            }
        }
    }
}
