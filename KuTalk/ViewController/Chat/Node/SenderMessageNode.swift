//
//  SenderMessageNode.swift
//  KuTalk
//
//  Created by Fu on 1/26/21.
//

import UIKit
import AsyncDisplayKit
import twitter_text

class SenderMessageNode: ASCellNode {
    
    var message = Message()
    let width = UIScreen.main.bounds.size.width - 100
    var didTapMessageContent:(() -> Void)?
    //MARK: - initial
    
    init(message: Message) {
        super.init()
        self.message = message
        self.automaticallyManagesSubnodes = true
        style.minHeight = ASDimensionMake(45)
        backgroundColor = .clear
    }
    
    //MARK: - Private
    
    private lazy var senderNameTextNode = { () -> ASTextNode in
        let node = ASTextNode()
        node.attributedText = NSAttributedString(string: message.userName, attributes: ReceiverMessageNode.nameAttributes)
        return node
    }()
    
    private lazy var messageImageNode = { () -> ASNetworkImageNode in
        let node = ASNetworkImageNode.init()
        node.setURL(URL.init(string: self.message.content), resetToDefault: true)
        node.defaultImage = UIImage(named: "img_default_avatar")
        node.cornerRadius = 15
        node.addTarget(self, action: #selector(didTapMessage), forControlEvents: .touchUpInside)

        return node
    }()
    
    private lazy var backgroundMessageImage = { () -> ASImageNode in
        let node = ASImageNode.init()
        node.image = UIImageView.imageWithImage(image: UIImage.init(named: "bubble_received")!)
        node.style.minHeight = ASDimensionMake(34)
        node.style.minWidth = ASDimensionMake(34)
        
        return node
    }()
    
    
    private lazy var avatarImageNode = { () -> ASNetworkImageNode in
        let node = ASNetworkImageNode.init()
        node.style.preferredSize = CGSize.init(width: 30, height: 30)
        node.cornerRadius = 15
        if let url = URL(string: message.avatar) {
            node.url = url
        } else {
            node.image = UIImage(named: "img_default_avatar")
        }
        return node
    }()
    
    private lazy var messageTextNode = { () -> ASTextNode in
        let node = ASTextNode.init()
        let height = 1000
        let width = self.width
        node.delegate = self
        node.style.maxSize = CGSize.init(width: width, height: 1000)
        node.style.flexShrink = 1
        node.style.alignSelf = .start
        if message.content.isSingleEmoji {
            node.backgroundColor = .clear
            node.attributedText = NSAttributedString.init(string: self.message.content, attributes: ReceiverMessageNode.emojiAttributes)
            
        } else {
            node.attributedText = NSAttributedString.init(string: self.message.content, attributes: ReceiverMessageNode.sendTextAttributes)
            node.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            
            node.backgroundColor = UIColor.appColor(.messageSender)
            let text: NSString = NSString(string: message.content)
            let updateString = NSMutableAttributedString(string: message.content, attributes: ReceiverMessageNode.sendTextAttributes)
            let entities = TwitterText.entities(inText: text as String)
            var tag: String = ""
            var tags: [String] = [String]()
            
            for entity:TwitterTextEntity in entities  {
                
                let r: NSRange = entity.range
                
                if entity.type == TwitterTextEntityType.URL {
                    tag = text.substring(with: r)
                    print(tag)
                    let url = URL(string: tag)
                    if url != nil {
                        var mutableActiveLinkAttributes: [AnyHashable : Any] = [:]
                        mutableActiveLinkAttributes[NSAttributedString.Key.foregroundColor] = UIColor(hex: "0044CC")
                        
                        if let url = URL(string: url?.absoluteString ?? "") {
                            mutableActiveLinkAttributes[tag] = url
                            tags.append(tag)
                        }
                        
                        if let mutableActiveLinkAttributes = mutableActiveLinkAttributes as? [NSAttributedString.Key : Any] {
                            updateString.addAttributes(mutableActiveLinkAttributes, range: r)
                        }
                    }
                }
            }
            if tags.count > 0 {
                node.linkAttributeNames = tags
                node.isUserInteractionEnabled = true
            } else {
                node.isUserInteractionEnabled = false
            }
            node.attributedText = updateString
        }
        node.addTarget(self, action: #selector(didTapMessage), forControlEvents: .touchUpInside)
        node.clipsToBounds = true
        node.cornerRadius = 4
        return node
    }()
    
    @objc private func didTapMessage() {
        didTapMessageContent?()
    }
    
    private func messageContentAttributes() {
        
        
    }
    
    override func layout() {
        super.layout()
        messageTextNode.cornerRadius = 6
    }
    
}

//MARK: SenderMessage Extention

extension SenderMessageNode {
    
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        
        //        let insetLayout = ASInsetLayoutSpec.init(insets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10), child: messageTextNode)
        //        let messageLayout = ASBackgroundLayoutSpec.init(child: insetLayout, background: backgroundMessageImage)
        //        messageLayout.style.alignSelf = .start
        //        var stackLayout: ASStackLayoutSpec
        if message.type == "photo" {
            messageImageNode.style.preferredSize = CGSize.init(width: 260, height: 300)
            return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: .infinity, bottom: 10, right: 10), child: messageImageNode)
        }
        //        let contentLayout = ASStackLayoutSpec(direction: .vertical,
        //                                              spacing: 4,
        //                                              justifyContent: .start,
        //                                              alignItems: .start,
        //                                              children: [senderNameTextNode, messageTextNode])
        //        messageImageNode.style.preferredSize = CGSize.init(width: 260, height: 300)
        //       let stackLayout = ASStackLayoutSpec.init(direction: .horizontal,
        //                                             spacing: 5,
        //                                             justifyContent: .end,
        //                                             alignItems: .end,
        //                                             children: [contentLayout, avatarImageNode])
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: .infinity, bottom: 10, right: 10), child: messageTextNode)
        //        return ASInsetLayoutSpec.init(insets: UIEdgeInsets(top: 0, left: 16, bottom: 16, right: 0), child: stackLayout)
        //        return stackLayout
    }
    
    static var textAttributes: [NSAttributedString.Key: Any] {
        return [NSAttributedString.Key.foregroundColor: UIColor.white,
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
    }
}


extension SenderMessageNode: ASTextNodeDelegate {
    func textNode(_ textNode: ASTextNode!, tappedLinkAttribute attribute: String!, value: Any!, at point: CGPoint, textRange: NSRange) {
        if let tag = attribute{
            if let url = URL(string: tag) {
                UIApplication.shared.open(url)
            }
        }
    }
}
