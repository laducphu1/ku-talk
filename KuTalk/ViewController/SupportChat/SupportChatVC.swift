//
//  SupportChatVC.swift
//  KuTalk
//
//  Created by Fu on 2/3/21.
//

import UIKit
import AsyncDisplayKit
import SVProgressHUD

class SupportChatVC: UIViewController {
    
    @IBOutlet weak var pinnedView: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    var groupId: String = ""
    var groupName: String = ""
    private var items = [Message]()
    @IBOutlet weak var nodeContainerView: UIView!
    private var tableNode: ASTableNode!
    @IBOutlet weak var pinMessageLabel: UILabel!
    @IBOutlet weak var inputTextView: EmojiTextField!
    var pinnedMessage: Message?
    @IBOutlet weak var chatInputView: UIView!
    private var ids = [String]()
    private let placeholderString = "Nhập tin nhắn..."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchData()
        fetchPinnedMessage()
        inputTextView.languageCode = "vi-VN"
        fetchMutedUser()
        removedMessage()
        registrationIds()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = groupName
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    private func textInputMode() -> UITextInputMode? {
        for mode in UITextInputMode.activeInputModes {
            if mode.primaryLanguage != "emoji" {
                return mode
            }
        }
        return nil
    }
    
    private func registrationIds() {
        Room.registrationIds(roomId: groupId) { [weak self] (ids, _) in
            self?.ids = ids
        }
    }
    
    private func blockUser(userId: String) {
        guard let user = AppDataSingleton.shared.currentUser else {return}
        user.blockUser(id: groupId, userId: userId)
    }
    
    private func muteUser(userId: String) {
        guard let user = AppDataSingleton.shared.currentUser else {return}
        user.muteUser(id: groupId, userId: userId)

    }
    
    private func composeMessage(type: MessageType, content: String)  {
        guard let user = AppDataSingleton.shared.currentUser else {
            return
        }
        let message = Message()
        message.type = type.rawValue
        message.content = content
        message.userId = user.phone
        message.userName = user.userName
        message.timestamp = String(Date().timeIntervalSince1970)
        message.avatar = user.avatar
        message.isAdmin = user.isAdmin
        message.send(toID: groupId) { [weak self] (status) in
            if status {
                if let self = self {
                    PushNotification.shared.sendNotification(message: message, ids: self.ids, groupName: self.groupName)

                }
            }
        }
        
    }
    private func pinMessage(message: Message) {
        message.pindMessage(toID: groupId) { (_) in
            
        }
    }
    
    private func delete(message: Message) {
        message.delete(toID: groupId) { (_) in
            
        }
    }
    
    private func configureUI() {
        inputTextView.textContainerInset = UIEdgeInsets(top: 15, left: 5, bottom: 15, right: 50)
        inputTextView.text = placeholderString
        inputTextView.textColor = UIColor.appColor(.blackTextColor).withAlphaComponent(0.8)
        
        nodeContainerView.backgroundColor = .clear
        tableNode = ASTableNode.init()
        nodeContainerView.addSubview(tableNode.view)
        tableNode.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableNode.view.topAnchor.constraint(equalTo: nodeContainerView.topAnchor),
            tableNode.view.leadingAnchor.constraint(equalTo: nodeContainerView.leadingAnchor),
            tableNode.view.trailingAnchor.constraint(equalTo: nodeContainerView.trailingAnchor),
            tableNode.view.bottomAnchor.constraint(equalTo: nodeContainerView.bottomAnchor),
            
        ])
        
        
        //        tableNode.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: nodeContainerView.frame.size.height)
        tableNode.view.tableFooterView = UIView.init()
        tableNode.view.backgroundColor = UIColor(hex: "F4F5F6")
        tableNode.delegate = self
        tableNode.dataSource = self
        tableNode.view.separatorStyle = .none
        tableNode.view.rowHeight = UITableView.automaticDimension
        tableNode.backgroundColor = UIColor.appColor(.tableViewBackground)
        var contentInset = tableNode.contentInset
        contentInset.bottom = 20
        contentInset.top = 60
        tableNode.contentInset = contentInset
        tableNode.view.showsVerticalScrollIndicator = false
        if let user = AppDataSingleton.shared.currentUser {
            cameraButton.isHidden = !user.isAdmin
        }
    }
    
    private func fetchData() {
        Message.fetchAllMessages(roomId: groupId) { [weak self] (data, error) in
            self?.items.append(data)
            let sortedArray = self?.items
            self?.items = sortedArray!.sorted {$0.timestamp < $1.timestamp }
            DispatchQueue.main.async {
                if let state = self?.items.isEmpty, state == false {
                    self?.tableNode.reloadData()
                    self?.tableNode.scrollToRow(at: IndexPath.init(row: (self?.items.count)! - 1, section: 0), at: .bottom, animated: false)
                }
            }
            
        }
    }
    
    private func removedMessage() {
        Message.removedMessage(roomId: groupId) { [weak self] (data, error) in
            guard let self = self else {return}
            if let index = self.items.firstIndex(where: {$0.messageId == data.messageId}) {
                DispatchQueue.main.async {
                    self.items.remove(at: index)
                    self.tableNode.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                }
            }
        }

    }
    
    private func fetchMutedUser() {
        guard let user = AppDataSingleton.shared.currentUser else {return}
        Message.muteList(roomId: groupId) { [weak self] (userId, _) in
            if userId == user.phone {
                self?.chatInputView.isHidden = true

            }
        }
    }
    
    private func fetchPinnedMessage() {
        Message.fetchPinnedMessage(roomId: groupId) { [weak self] (data, error) in
            guard let self = self else {return}
            if let data = data {
                self.pinnedMessage = data
                self.pinnedView.isHidden = false
                if data.type == "photo" {
                    self.pinMessageLabel.text = "Photo"
                } else {
                    self.pinMessageLabel.text = data.content
                }
            } else {
                self.pinnedView.isHidden = true
            }
        }
    }
    
    //MARK: - Actions
    
    @IBAction func clearPinAction(_ sender: Any) {
        self.pinnedView.isHidden = true
        Message.removePinMessage(roomId: groupId) { (_, error) in
            if let error = error {
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                SVProgressHUD.dismiss(withDelay: 1.0)
            }
        }
    }
    
    @IBAction func cameraAction(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        if let text = self.inputTextView.text, text != placeholderString {
            if text.count > 0 {
                let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                let matches = detector.matches(in: text, options: [], range: NSRange(location: 0, length: text.utf16.count))
                if matches.count > 0 {
                    SVProgressHUD.showError(withStatus: "Bạn không thể gửi link trong nhóm chat này!")
                    SVProgressHUD.dismiss(withDelay: 1.0)
                    return
                }
                self.composeMessage(type: .text, content: text.trimmingCharacters(in: .whitespacesAndNewlines))
                self.inputTextView.text = ""
            }
        }
    }
    
    @IBAction func selectPinViewAction(_ sender: Any) {
        guard let pinMessage = pinnedMessage else {return}
        if let index = items.firstIndex(where: {$0.messageId == pinMessage.messageId}) {
            self.tableNode.scrollToRow(at: IndexPath(row: index, section: 0), at: .middle, animated: true)
        }
    }
    @IBAction func emojiKeyboard(_ sender: Any) {
        if inputTextView.languageCode == "emoji" {
            inputTextView.languageCode = "vi-VN"
            
        } else {
            inputTextView.languageCode = "emoji"
            
        }
    }
}

//MARK: - ASTableDelegate + ASTableDatasource

extension SupportChatVC: ASTableDelegate, ASTableDataSource {
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: ASTableView, nodeForRowAt indexPath: IndexPath) -> ASCellNode {
        let message = self.items[indexPath.row]
        if message.userId == AppDataSingleton.shared.currentUser!.phone {
            let node = SenderMessageNode.init(message: message)
            node.selectionStyle = .none
            return node
        }
        let node = ReceiverMessageNode.init(message: message)
        node.didSelectAvatarImage = { [weak self] in
            guard let self = self else {
                return
            }
            let alert = UIAlertController(title: "Lựa chọn", message: "", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Cấm chat", style: .default, handler: {[weak self] (_) in
                self?.muteUser(userId: message.userId)
            }))
            alert.addAction(UIAlertAction(title: "Kick ra khỏi phòng", style: .default, handler: { [weak self](_) in
                self?.blockUser(userId: message.userId)
                
            }))
            alert.addAction(UIAlertAction(title: "Huỷ", style: .destructive, handler: { (_) in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
        node.selectionStyle = .none
        return node
    }
    func tableNode(_ tableNode: ASTableNode, didSelectRowAt indexPath: IndexPath) {
        //only admin
        guard let user = AppDataSingleton.shared.currentUser, user.isAdmin else {return}
        let message = self.items[indexPath.row]
        let alert = UIAlertController(title: "Lựa chọn", message: "", preferredStyle: .actionSheet)

        if message.isAdmin {
            alert.addAction(UIAlertAction(title: "Ghim tin nhắn", style: .default, handler: { (_) in
                self.pinMessage(message: message)
            }))
        }
        alert.addAction(UIAlertAction(title: "Xoá tin nhắn", style: .default, handler: { (_) in
            self.delete(message: message)
        }))
        
        alert.addAction(UIAlertAction(title: "Huỷ", style: .destructive, handler: { (_) in
            
        }))
        present(alert, animated: true, completion: nil)
    }
}


extension SupportChatVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeholderString {
            textView.textColor = UIColor.appColor(.blackTextColor)
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.count == 0 {
            textView.text = placeholderString
            textView.textColor = UIColor.appColor(.blackTextColor).withAlphaComponent(0.8)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textView.sizeToFit()
        print(nodeContainerView.frame)
        print(tableNode.frame)
        tableNode.performBatchUpdates(nil, completion: nil)
        tableNode.scrollToRow(at: IndexPath(row: items.count - 1, section: 0), at: .bottom, animated: true)
        //        scrollViewHeightConstraint.constant = fixedHeight + collectionViewHeightConstraint.constant + textViewHeightConstraint.constant
    }
}
extension SupportChatVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage {
            image.uploadImage { (urlString, error) in
                if !urlString.isEmpty {
                    self.composeMessage(type: .photo, content: urlString)
                    
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
