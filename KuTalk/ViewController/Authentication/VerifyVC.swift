//
//  VerifyVC.swift
//  KuTalk
//
//  Created by flydino on 1/26/21.
//

import UIKit
import FirebaseAuth
import SVProgressHUD
import ISEmojiView

class VerifyVC: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var codeTF: UITextField!
    @IBOutlet weak var resendCodeButton: UIButton!
    @IBOutlet weak var resendCodeLabel: UILabel!
    
    var phone = ""
    var userName = ""
    var avatarImage: UIImage?
    var isLogin = true
    
    private var verificationID: String?
    private var countTimer: Timer!
    private var counter = 60
    private let keyboardSettings = KeyboardSettings(bottomType: .categories)
    private var emojiView: EmojiView!

    //MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        sendRequestGetCode()
    }
    
    //MARK: - Helper
    
    private func setupUI() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        contentView.addShadow(ofColor: .gray, radius: 3, offset: .zero, opacity: 1)
        emojiView = EmojiView(keyboardSettings: keyboardSettings)
        emojiView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func createUser(completion: @escaping (Error?) -> ()) {
        let user = User()
        user.phone = phone
        user.userName = userName
        user.avatarImage = avatarImage
        user.saveUser { [weak self] (error) in
            guard let self = self else { return }
            if let error = error {
                completion(error)
            } else {
                self.updatePushNotification(user: user)
                Conversation.addInfomartionConversation(roomID: self.phone)
                completion(nil)
            }
        }
    }
    
    @objc private func changeTitle() {
         if counter != 0 {
            resendCodeButton.isUserInteractionEnabled = false
            counter -= 1
            let second = counter < 10 ? "0\(counter)" : "\(counter)"
            resendCodeLabel.text = "Gửi lại mã trong: 00:\(second)"
         } else {
            resendCodeButton.isUserInteractionEnabled = true
            countTimer.invalidate()
            resendCodeLabel.text = "Gửi lại mã OTP"
         }
    }
    
    private func startCountDown() {
        resendCodeButton.isHidden = false
        resendCodeLabel.isHidden = false
        counter = 60
        self.countTimer = Timer.scheduledTimer(timeInterval: 1,
                                                     target: self,
                                                     selector: #selector(self.changeTitle),
                                                     userInfo: nil,
                                                     repeats: true)
    }
    
    private func updatePushNotification(user: User) {
        if let fcmToken = AppDataSingleton.shared.pushNotificationID {
            user.pushNotificationId = fcmToken
            AppDataSingleton.shared.currentUser = user
            user.updateNotificationID(with: fcmToken)
        }
    }
    
    //MARK: - Action
    
    @IBAction func didClickVerify(_ sender: Any) {
        verifyPhoneNumber()
    }
    
    @IBAction func didClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didClickResendCode(_ sender: Any) {
        sendRequestGetCode()
    }
    
    //MARK: - Firebase
    
    private func sendRequestGetCode() {
        phone = AppDataSingleton.shared.handlePhoneNumber(phone)
        Auth.auth().languageCode = "vi_VN"
        Auth.auth().settings?.isAppVerificationDisabledForTesting = true
        SVProgressHUD.show()
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { [weak self] (verificationID, error) in
            SVProgressHUD.dismiss()
            guard let self = self else { return }
            if let error = error {
                DispatchQueue.main.async {
                    self.showAlert(title: "Thông báo", message: error.localizedDescription)
                }
                return
            } else {
                self.verificationID = verificationID
                DispatchQueue.main.async {
                    self.showAlert(title: "Thông báo", message: "Đã gửi yêu cầu gửi mã xác nhận đến số điện thoại của bạn")
                }
                self.startCountDown()
            }
        }
    }
    
    private func verifyPhoneNumber() {
        guard let code = codeTF.text,
           code != "" else {
            showAlert(title: "Thông báo", message: "Vui lòng nhập mã xác nhận!")
            return
        }
        if let verificationID = verificationID  {
            SVProgressHUD.show()
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID, verificationCode: code)
            Auth.auth().signIn(with: credential) { (authData, error) in
                if error != nil {
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.showAlert(title: "Thông báo", message: "Mã xác nhận không chính xác!")
                    }
                } else {
                    if self.isLogin {
                        User.shared.getUserInfo(self.phone) { (user) in
                            SVProgressHUD.dismiss()
                            if let user = user {
                                self.updatePushNotification(user: user)
                                DispatchQueue.main.async {
                                    AppDataSingleton.shared.setInitialVC()
                                }
                            } else {
                                DispatchQueue.main.async {
                                    self.showAlert(title: "Thông báo", message: "Đã có lỗi xảy ra. Vui lòng thử lại!")
                                }
                            }
                        }
                    } else {
                        self.createUser { (error) in
                            SVProgressHUD.dismiss()
                            if let error = error {
                                DispatchQueue.main.async {
                                    self.showAlert(title: "Thông báo", message: error.localizedDescription)
                                }
                            } else {
                                DispatchQueue.main.async {
                                    AppDataSingleton.shared.setInitialVC()
                                }
                            }
                        }
                    }
                }
            }
        } else {
            showAlert(title: "Thông báo", message: "Đã có lỗi xảy ra. Vui lòng thử lại!")
        }
    }
}
