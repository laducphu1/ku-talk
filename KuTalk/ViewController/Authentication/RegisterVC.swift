//
//  RegisterVC.swift
//  KuTalk
//
//  Created by flydino on 1/26/21.
//

import UIKit
import FirebaseStorage

class RegisterVC: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField!
    
    private var imageSelected: UIImage?
    
    //MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - Helper
    
    private func setupUI() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        contentView.addShadow(ofColor: .gray, radius: 3, offset: .zero, opacity: 1)
    }
    
    private func showVerifyVC() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyVC") as? VerifyVC {
            vc.avatarImage = imageSelected
            vc.phone = phoneTF.text!
            vc.userName = userNameTF.text!
            vc.isLogin = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func openCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func openGallary() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func showAlertPickImage() {
        let alert = UIAlertController(title: "Chọn avatar", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Thư viện ảnh", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Hủy", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Action
    
    @IBAction func didClickRegister(_ sender: Any) {
        showVerifyVC()
    }
    
    @IBAction func didClickLogin(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didClickAvatar(_ sender: Any) {
        showAlertPickImage()
    }
}

//MARK: - ImagePickerDelegate

extension RegisterVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.editedImage] as? UIImage {
            avatarImageView.image = image
            imageSelected = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
