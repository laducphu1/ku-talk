//
//  LoginVC.swift
//  KuTalk
//
//  Created by flydino on 1/26/21.
//

import UIKit
import SVProgressHUD

class LoginVC: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var phoneTF: UITextField!
    
    //MARK: - View LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - Helper
    
    private func setupUI() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        contentView.addShadow(ofColor: .gray, radius: 3, offset: .zero, opacity: 1)
        AppDataSingleton.shared.updateInterfaceStyle()
    }
    
    private func showVerifyVC() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "VerifyVC") as? VerifyVC,
           let phone = phoneTF.text {
            vc.phone = phone
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    //MARK: - Action
    
    @IBAction func didClickLogin(_ sender: Any) {
        if var phone = phoneTF.text, phone.isEmpty == false {
            phone = AppDataSingleton.shared.handlePhoneNumber(phone)
            checkExistUser(with: phone)
        } else {
            self.showAlert(message: "Vui lòng nhập số điện thoại")
        }
    }
    
    @IBAction func didClickRegister(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as? RegisterVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: - Firebase
    
    private func checkExistUser(with phone: String) {
        SVProgressHUD.show()
        User.shared.checkExistUser(phone: phone) { (isExist) in
            if isExist {
                SVProgressHUD.dismiss()
                self.showVerifyVC()
            } else {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.showAlert(message: "Số điện thoại chưa được đăng ký")
                }
            }
        }
    }
}
