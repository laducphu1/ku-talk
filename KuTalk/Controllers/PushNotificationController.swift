//
//  PushNotificationController.swift
//  KuTalk
//
//  Created by Fu on 2/1/21.
//

import UIKit
import Messages
import Firebase
import Alamofire
import SwiftyJSON

class PushNotification: NSObject {
    
    static let shared = PushNotification()
    
    let legacyKey = "key=AAAAa7wHIBc:APA91bGr2IoLNx9x60uS9WluAqqSz310viing9VT8txx4sO-yCbvEHlgMbvBgpm2bbyCrSfOWnli9gydd6b2w4Pf3XoOq8DjzQA0agZK6W68Td3AEnoxX2VXU_9NHKTclGGPwU9Co32K"
    var emailSignIn: String?
    var IsNotNeedPresentChatViewController = false
    
    //MARK: - Lifecycle
    
    override init() {
        super.init()

    }
    
 
    deinit {
        
    }
    
    //MARK: - Helper
    
  
    
    //MARK: - Prepare for send notification

 
    
    func sendNotification(message: Message, ids: [String], groupName: String) {
        
        let idsList = ids.filter{ $0 != AppDataSingleton.shared.currentUser?.pushNotificationId }
        
        let bodyString = message.type == "photo" ? "\(message.userName) đã gửi 1 ảnh": "\(message.userName): \(message.content)"
        let title = groupName //"\(groupName)"
        let data: [String: Any] = ["userId": message.userId]
        PushNotification.shared.postNotificationWith(title: title, body: bodyString, data: data, registration_ids: idsList, { (response, status) in
            if status == 0 {
                print("can't send notification")
            }
        })

    }
 
    //MARK: - Registe NOtification

    
    func postNotificationWith(title: String,
                              body: String,
                              data: [String:Any],
                              registration_ids: [String],
                              _ completionHandler: (( _ response: String?,_ status: Int) -> Void)?) {
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let headers: HTTPHeaders = ["Content-Type": "application/json",
                       "Authorization": legacyKey]
        let params : [String: Any] = ["registration_ids": registration_ids,
                                      "priority":"high",
                                      "data":data,
                                      "notification": ["body": body,
                                                       "title": title,
                                                       "sound":"chime.aiff"]]
        AF.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            switch response.result {
            
            case .success(let result as [String:Any]):
                
                print("success")
                
                
            case .failure(let error):
                    print(error)
            case .success(_):
                break
                
            }
            
        }
       
    
    }
    
    private func requestWithPathService(_ method: HTTPMethod, path: String, parameter: [String : Any]?, setHeaders: [String : String]?, completionHandler: ((_ errorRequest: AnyObject?, _ responseObject: NSDictionary?) -> Void)?){
        print(path)
        let headers: HTTPHeaders = ["Content-Type": "application/json",
                                    "Authorization": "key= \(legacyKey)"]
        AF.request(path, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            switch response.result {
            
            case .success(let result as [String:Any]):
                
                let success = result["success"] as! Bool
                
                
            case .failure(let error):
                    print(error)
            case .success(_):
                break
                
            }
            
        }
       
    }
}

