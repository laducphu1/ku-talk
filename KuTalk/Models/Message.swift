//
//  Message.swift
//  KuTalk
//
//  Created by Fu on 1/26/21.
//

import UIKit
import ObjectMapper

enum MessageType :String {
    case photo = "photo"
    case text = "text"
}

class Message : Mappable {
    var content : String = ""
    var reference : String = ""
    var userId : String = ""
    var userName : String = ""
    var timestamp : String = ""
    var type : String = ""
    var avatar: String = ""
    var messageId: String = ""
    var roomId: String = ""
    var isAdmin: Bool = false

    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        content <- map["content"]
        reference <- map["reference"]
        userId <- map["userId"]
        userName <- map["userName"]
        type <- map["type"]
        timestamp <- map["timestamp"]
        type <- map["type"]
        avatar <- map["avatar"]
        avatar <- map["avatar"]
        roomId <- map["roomId"]
        messageId <- map["messageId"]
        isAdmin <- map["isAdmin"]

    }
    
    static func fetchAllMessages(roomId: String, completionHandler: @escaping (_ arrayGet: Message, _ error: Error?) -> ()) {
        
        AppDataSingleton.shared.ref.child("Messages").child(roomId).observe(.childAdded) { (snapshot) in
            
            if let value = snapshot.value as? [String: Any] {
                if let model = Message(JSON: value) {
                    completionHandler(model, nil)
                }
                
            } else {
            }
        }
    }
    
    static func removedMessage(roomId: String, completionHandler: @escaping (_ arrayGet: Message, _ error: Error?) -> ()) {
        
        AppDataSingleton.shared.ref.child("Messages").child(roomId).observe(.childRemoved) { (snapshot) in
            
            if let value = snapshot.value as? [String: Any] {
                if let model = Message(JSON: value) {
                    completionHandler(model, nil)
                }
                
            } else {
            }
        }
    }
    
    static func muteList(roomId: String, completionHandler: @escaping (_ arrayGet: String, _ error: Error?) -> ()) {
        
        AppDataSingleton.shared.ref.child("Messages").child(roomId).child("muteList").observe(.value) { (snapshot) in
            
            if let value = snapshot.value as? String {
                completionHandler(value, nil)
                
            } else {
            }
        }
    }
    
    static func muteUser(roomId: String, completionHandler: @escaping (_ arrayGet: String, _ error: Error?) -> ()) {
        
//        AppDataSingleton.shared.ref.child("Messages").child(roomId).child("muteList").updateChildValues(<#T##values: [AnyHashable : Any]##[AnyHashable : Any]#>)
        AppDataSingleton.shared.ref.child("Messages").child(roomId).child("muteList").observe(.childAdded) { (snapshot) in
            
            if let value = snapshot.value as? String {
                completionHandler(value, nil)
                
            } else {
            }
        }
    }
    
    static func fetchPinnedMessage(roomId: String, completionHandler: @escaping (_ arrayGet: Message?, _ error: Error?) -> ()) {
        
        AppDataSingleton.shared.ref.child("Rooms").child(roomId).child("PinnedMessage").observe(.value) { (snapshot) in
            
            if let value = snapshot.value as? [String: Any] {
                if let model = Message(JSON: value) {
                    completionHandler(model, nil)
                }
                
            } else {
                completionHandler(nil, nil)
            }
        }
    }
    
    static func removePinMessage(roomId: String, completionHandler: @escaping (_ arrayGet: Message?, _ error: Error?) -> ()) {
        
        AppDataSingleton.shared.ref.child("Rooms").child(roomId).child("PinnedMessage").removeValue { (error, ref) in
            completionHandler(nil, error)
        }
    }
    
    func send(toID: String, completion: @escaping (Bool) -> Swift.Void)  {
        uploadMessageWithName(name: toID) { (error) in
            if error == nil {
                completion(true)
            } else {
                completion(false)
            }
            
        }
    }
    
    func pindMessage(toID: String, completion: @escaping (Bool) -> Swift.Void)  {
        AppDataSingleton.shared.ref.child("Rooms").child(toID).child("PinnedMessage").setValue(toDictionary()) { (error, ref) in
            if error != nil {
                completion(false)
            } else {
                completion(true)
            }
        }
    }
    
    func delete(toID: String, completion: @escaping (Bool) -> Swift.Void)  {
        AppDataSingleton.shared.ref.child("Messages").child(toID).child(messageId).removeValue { (error, ref) in
//            completionHandler(false)
        }
    }
    
    func update(to idRoom: String, completion: @escaping (Error?) -> ()) {
        AppDataSingleton.shared.ref.child("Messages/\(idRoom)/\(messageId)/content").setValue(content) { (error, ref) in
            completion(error)
        }
    }
    
    private func uploadMessageWithName(name: String, completionHandler: @escaping (_ error: Error?) -> ()) {
        let time = Int(NSDate().timeIntervalSince1970 * 1000)
        reference = "-" + String(time)
        self.messageId = reference
        AppDataSingleton.shared.ref.child("Messages").child(name).child(reference).setValue(toDictionary()) { (error, ref) in
            if let error = error {
                completionHandler(error)
            } else {
                completionHandler(nil)
            }
        }
    }
    func toDictionary() -> [String:Any] {
        
        var dictionary = [String:Any]()
        dictionary["avatar"] = avatar
        
        dictionary["content"] = content
        
        dictionary["messageId"] = messageId
        
        dictionary["roomId"] = roomId
        
        dictionary["timestamp"] = timestamp
        
        dictionary["type"] = type
        
        dictionary["userName"] = userName
        dictionary["userId"] = userId
        dictionary["isAdmin"] = isAdmin
        return dictionary
    }
    
    
    
}
