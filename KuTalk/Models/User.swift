import Foundation
import ObjectMapper
import FirebaseStorage
import Messages
import Firebase

class User : Mappable {
    var userName : String = ""
    var phone : String = ""
    var avatar : String = ""
    var avatarImage: UIImage?
    var isAdmin = false
    var pushNotificationId: String = ""
    var blocks: [String] = [String]()
    var mutes: [String] = [String]()
    var forCheckUpdate: Bool = false

    static let shared = User()
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        userName <- map["userName"]
        phone <- map["phoneNumber"]
        avatar <- map["avatar"]
        isAdmin <- map["isAdmin"]
        blocks <- map["blocks"]

        mutes <- map["mutes"]

        pushNotificationId <- map["pushNotificationId"]
        forCheckUpdate <- map["forCheckUpdate"]

    }
    
    func toDictionary() -> [String: Any] {
        var data: [String: Any] = [:]
        data["userName"] = userName
        data["phoneNumber"] = phone
        data["avatar"] = avatar
        data["pushNotificationId"] = pushNotificationId
        data["isAdmin"] = isAdmin
        data["blocks"] = blocks
        data["mutes"] = mutes
        data["forCheckUpdate"] = forCheckUpdate

        return data
    }
    
    
    //MARK: - Firebase
    
//    func observeBlockRooms() {
//        AppDataSingleton.shared.ref.child("User/\(phone)/blocks").observe(.value) { (snapshot) in
//            var blocksList = [String]()
//            if let blocks = snapshot.value as? [String] {
//                blocksList = blocks
//            }
//            let user = AppDataSingleton.shared.currentUser
//            user?.blocks = blocksList
//            AppDataSingleton.shared.currentUser = user
//        }
//    }
    
    func saveUser(completion: @escaping (Error?) -> ()) {
        AppDataSingleton.shared.ref.child("User").child(phone).setValue(self.toDictionary()) { (error, dataRef) in
            if let error = error {
                completion(error)
            } else {
                AppDataSingleton.shared.currentUser = self
                if let img = self.avatarImage {
                    self.uploadImage(image: img) { (url) in
                        if let url = url {
                            self.avatar = url.absoluteString
                        }
                    }
                }
                completion(nil)
            }
        }
    }
    
    func updateUserInfo(completion: @escaping (Error?) -> ()) {
        AppDataSingleton.shared.ref.child("User").child(phone).setValue(self.toDictionary()) { (error, dataRef) in
            if let error = error {
                completion(error)
            } else {
                completion(nil)
            }
        }
    }
    
    func blockUser(id: String, userId: String) {
        getUserInfo(userId) { (user) in
            if let user = user {
                if !user.blocks.contains(id) {
                    user.blocks.append(id)
                }
                user.forCheckUpdate = !user.forCheckUpdate
                user.updateUserInfo { (error) in
                    if error == nil {
                        Room.addUserToBlocksOfRoom(id, userID: userId) { (_) in
                            
                        }
                    }
                }
            }
        }
    }
    
    func muteUser(id: String, userId: String) {
        getUserInfo(userId) { (user) in
            if let user = user {
                if !user.mutes.contains(id) {
                    user.mutes.append(id)
                }
                user.forCheckUpdate = !user.forCheckUpdate
                user.updateUserInfo { (error) in
                    if error == nil {
                        Room.addUserToMuteOfRoom(id, userID: userId) { (_) in
    
                        }
                    }
                }
            }
        }
    }
    
    private func subscribeTopic() {
        Messaging.messaging().subscribe(toTopic: "Group1") { (error) in
            
        }
    }
    
    func uploadImage(image: UIImage, completion: @escaping (URL?) -> ()) {
        guard let data = image.jpegData(compressionQuality: 0.3) else {
            return
        }
        let ref = AppDataSingleton.shared.storageRef.child("Avatar/hi")
        
        ref.putData(data, metadata: nil) { (metadata, error) in
            guard metadata != nil else {
                completion(nil)
                return
            }
            ref.downloadURL { (url, error) in
                guard let url = url else {
                    completion(nil)
                    return
                }
                self.updateAvatarURL(url.absoluteString) { (error) in
                    if error != nil {
                        completion(nil)
                    } else {
                        self.avatar = url.absoluteString
                        AppDataSingleton.shared.currentUser = self
                        completion(url)
                    }
                }
            }
        }
    }
    
    func updateAvatarURL(_ url: String, completion: @escaping (Error?) -> ()) {
        AppDataSingleton.shared.ref.child("User/\(phone)/avatar").setValue(url) { (error, dataRef) in
            if let error = error {
                completion(error)
            } else {
                completion(nil)
            }
        }
    }
    
    func updateUserName(_ userName: String, completion: @escaping (Error?) -> ()) {
        AppDataSingleton.shared.ref.child("User/\(phone)/userName").setValue(userName) { (error, dataRef) in
            if let error = error {
                completion(error)
            } else {
                self.userName = userName
                AppDataSingleton.shared.currentUser = self
                completion(nil)
            }
        }
    }
    
    func checkExistUser(phone: String, completion: @escaping (Bool) -> ()) {
        AppDataSingleton.shared.ref.child("User/\(phone)").observeSingleEvent(of: .value) { (snapshot) in
            if ((snapshot.value as? [String: Any]) != nil) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    func getUserInfo(_ phone: String, completion: @escaping (User?) -> ()) {
        AppDataSingleton.shared.ref.child("User/\(phone)").observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? [String: Any] {
                let user = User(JSON: dict)
                completion(user)
            } else {
                completion(nil)
            }
        }
    }
    
    static func obseverUser( completion: @escaping (User?) -> ()) {
        guard let user = AppDataSingleton.shared.currentUser else {return}
        AppDataSingleton.shared.ref.child("User").child(user.phone).observe(.value) { (snapshot) in
            User().getUserInfo(user.phone) { (user) in
                if let user = user {
                    AppDataSingleton.shared.currentUser = user
                    completion(user)
                }
            }
        }
    }
    

    static func fetchAdminFcmIdsList(completion: @escaping ([String]?, Error?) -> ()) {
        AppDataSingleton.shared.ref.child("Admin").observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? [String: String] {
                let values = Array(dict.values)
                completion(values, nil)
            } else {
                let error = NSError(domain: "HttpResponseErrorDomain", code: 404, userInfo: [NSLocalizedDescriptionKey: "Đã có lỗi xảy ra"])
                completion(nil, error)
            }
        }
    }
    
    func removeBlockFromRoom(_ roomID: String, userID: String, completion: @escaping (Error?) -> ()) {
        self.getUserInfo(userID) { (user) in
            if let user = user, user.blocks.count > 0 {
                var blockList = user.blocks
                let index = blockList.firstIndex(of: roomID)
                if let index = index {
                    blockList.remove(at: index)
                    AppDataSingleton.shared.ref.child("User/\(userID)/blocks").setValue(blockList) { (error, ref) in
                        completion(error)
                        return
                    }
                } else {
                    let error = NSError(domain: "HttpResponseErrorDomain", code: 404, userInfo: [NSLocalizedDescriptionKey: "Đã có lỗi xảy ra"])
                    completion(error)
                }
            } else {
                let error = NSError(domain: "HttpResponseErrorDomain", code: 404, userInfo: [NSLocalizedDescriptionKey: "Đã có lỗi xảy ra"])
                completion(error)
            }
        }
    }
    
    func removeMuteFromRoom(_ roomID: String, userID: String, completion: @escaping (Error?) -> ()) {
        self.getUserInfo(userID) { (user) in
            if let user = user, user.mutes.count > 0 {
                var muteList = user.mutes
                let index = muteList.firstIndex(of: roomID)
                if let index = index {
                    muteList.remove(at: index)
                    AppDataSingleton.shared.ref.child("User/\(userID)/mutes").setValue(muteList) { (error, ref) in
                        completion(error)
                        return
                    }
                } else {
                    let error = NSError(domain: "HttpResponseErrorDomain", code: 404, userInfo: [NSLocalizedDescriptionKey: "Đã có lỗi xảy ra"])
                    completion(error)
                }
            } else {
                let error = NSError(domain: "HttpResponseErrorDomain", code: 404, userInfo: [NSLocalizedDescriptionKey: "Đã có lỗi xảy ra"])
                completion(error)
            }
        }
    }
    
    func updateNotificationID(with notificationID: String) {
        AppDataSingleton.shared.ref.child("Rooms/RoomId1/users/\(phone)")
            .setValue(notificationID)
        AppDataSingleton.shared.ref.child("Rooms/RoomId2/users/\(phone)")
            .setValue(notificationID)
        AppDataSingleton.shared.ref.child("Rooms/RoomId3/users/\(phone)")
            .setValue(notificationID)
        AppDataSingleton.shared.ref.child("User/\(phone)/pushNotificationId")
            .setValue(notificationID)
        if !isAdmin {
            AppDataSingleton.shared.ref.child("Messages/SupportRooms/\(phone)/users/\(phone)")
                .setValue(notificationID)
            AppDataSingleton.shared.ref.child("SupportRooms/\(phone)/pushNotificationId")
                .setValue(notificationID)
        } else {
            let data = [phone: notificationID]
            AppDataSingleton.shared.ref.child("Admin")
                .setValue(data)
        }
    }
}
