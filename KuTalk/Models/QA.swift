//
//  QA.swift
//  KuTalk
//
//  Created by Lê Phước on 1/27/21.
//

import Foundation
import ObjectMapper

class QA : Mappable {
    var answer : String = ""
    var question : String = ""
    
    static let shared = QA()
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        answer <- map["answer"]
        question <- map["question"]
    }
    
    func getAllQA(completion: @escaping ([QA]?) -> ()) {
        AppDataSingleton.shared.ref.child("FAQ").observeSingleEvent(of: .value) { (snapshot) in
            if let array = snapshot.value as? [Any] {
                var listQA = [QA]()
                array.forEach { (dict) in
                    if let dict = dict as? [String: Any],
                       let qa = QA(JSON: dict) {
                        listQA.append(qa)
                    }
                }
                completion(listQA)
            } else {
                completion(nil)
            }
        }
    }
}
