//
//  Conversation.swift
//  KuTalk
//
//  Created by Lê Phước on 2/1/21.
//

import Foundation
import ObjectMapper
import FirebaseDatabase
import SwiftyJSON

class Conversation : Mappable {
    
    var createdTime: Double = 0
    var lastMessageID: String = ""
    var modifiedTime: Double = 0
    var name: String = ""
    var roomId: String = ""
    var lastMessage: String = ""
    var lastMessageTime: Double {
        return -(Double(lastMessageID) ?? 0)
    }
    var pushNotificationId: String = ""
    
    static let shared = Conversation()
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        createdTime <- map["createdTime"]
        lastMessageID <- map["lastMessageID"]
        modifiedTime <- map["modifiedTime"]
        name <- map["name"]
        roomId <- map["roomId"]
        lastMessage <- map["lastMessage"]
        pushNotificationId <- map["pushNotificationId"]
    }
    

    func toDictionary() -> [String:Any] {
        
        var dictionary = [String:Any]()
        dictionary["createdTime"] = createdTime
        
        dictionary["lastMessageID"] = lastMessageID
        
        dictionary["modifiedTime"] = modifiedTime
        
        dictionary["name"] = name
        
        dictionary["roomId"] = roomId
        
        dictionary["lastMessage"] = lastMessage
        
        dictionary["pushNotificationId"] = pushNotificationId

        return dictionary
    }
    
    
    static func updateConversationFrom(message: Message) {
        let time = Int(NSDate().timeIntervalSince1970 * 1000)
        let conversation = Conversation()
        conversation.createdTime = Double(time)
        conversation.lastMessageID = message.messageId
        conversation.name  = "Support 24/7"
        conversation.roomId = message.userId
        conversation.lastMessage = message.type == "photo" ? "ảnh": message.content
        AppDataSingleton.shared.ref.child("SupportRooms").child(message.userId).setValue(conversation.toDictionary()) { (error, ref) in
            if let error = error {
                print(error)
            } else {
            }
        }
    }
    
    static func addInfomartionConversation(roomID: String) {
        let data = [
            "roomId": roomID,
            "name": "Support 24/7"
        ]
        AppDataSingleton.shared.ref.child("SupportRooms").child(roomID).updateChildValues(data, withCompletionBlock: { (error, ref) in
            if let error = error {
                print(error)
            } else {
            }
        })
    }
    
    func getConversations(completion: @escaping (Int) -> ()) {
        AppDataSingleton.shared.ref.child("SupportRooms").observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? [String: Any] {
                completion(dict.keys.count)
            } else {
                completion(0)
            }
        }
    }
    
    func getConversations(completion: @escaping ([Conversation]?, Error?) -> ()) {
        AppDataSingleton.shared.ref.child("SupportRooms").observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? [String: Any] {
                var conversations = [Conversation]()
                dict.keys.forEach { (key) in
                    if let conversationDict = dict[key] as? [String: Any] {
                        if let conversation = Conversation(JSON: conversationDict) {
                            conversations.append(conversation)
                        }
                    }
                }
                completion(conversations, nil)
            } else {
                let error = NSError(domain: "HttpResponseErrorDomain", code: 404, userInfo: [NSLocalizedDescriptionKey: "Đã có lỗi xảy ra"])
                completion(nil, error)
            }
        }
    }
    
    func getConversationsKey(completion: @escaping ([String]) -> ()) {
        AppDataSingleton.shared.ref.child("Messages").child("SupportRooms").observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? [String: Any] {
                completion(Array(dict.keys))
            } else {
                completion([])
            }
        }
    }
}
