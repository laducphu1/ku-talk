//
//  Room.swift
//  KuTalk
//
//  Created by Fu on 2/3/21.
//

import UIKit

class Room: NSObject {
    
    
    static func registrationIds(roomId: String, completionHandler: @escaping (_ arrayGet: [String], _ error: Error?) -> ()) {
        
        AppDataSingleton.shared.ref.child("Rooms").child(roomId).child("users").observe(.value) { (snapshot) in
            if let data = snapshot.value as? [String: String] {
                let ids = Array(data.values)
                completionHandler(ids, nil)
            }
        }
    }
    
    //MARK: - Block
    
    static func addUserToBlocksOfRoom(_ roomID: String, userID: String, completion: @escaping(Error?) -> ()) {
        AppDataSingleton.shared.ref.child("Rooms/\(roomID)/blocks/\(userID)").setValue(userID) { (error, ref) in
            completion(error)
        }
    }
    
    static func removeUserFromBlocksOfRoom(_ roomID: String, userID: String, completion: @escaping(Error?) -> ()) {
        AppDataSingleton.shared.ref.child("Rooms/\(roomID)/blocks/\(userID)").removeValue { (error, ref) in
            completion(error)
        }
    }
    
    static func getUsersBlockOfRoom(_ roomID: String, completion: @escaping ([String]) -> ()) {
        AppDataSingleton.shared.ref.child("Rooms/\(roomID)/blocks").observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? [String: Any] {
                completion(Array(dict.keys))
            } else {
                completion([String]())
            }
        }
    }
    
    //MARK: - Mute
    static func addUserToMuteOfRoom(_ roomID: String, userID: String, completion: @escaping(Error?) -> ()) {
        AppDataSingleton.shared.ref.child("Rooms/\(roomID)/mutes/\(userID)").setValue(userID) { (error, ref) in
            completion(error)
        }
    }
    
    static func removeUserFromMuteOfRoom(_ roomID: String, userID: String, completion: @escaping(Error?) -> ()) {
        AppDataSingleton.shared.ref.child("Rooms/\(roomID)/mutes/\(userID)").removeValue { (error, ref) in
            completion(error)
        }
    }
    
    static func getUsersMuteOfRoom(_ roomID: String, completion: @escaping ([String]) -> ()) {
        AppDataSingleton.shared.ref.child("Rooms/\(roomID)/mutes").observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? [String: Any] {
                completion(Array(dict.keys))
            } else {
                completion([String]())
            }
        }
    }
}
