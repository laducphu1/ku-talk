//
//  UIApplicationExtension.swift
//  KuTalk
//
//  Created by flydino on 1/26/21.
//

import Foundation
import UIKit

extension UIApplication {
    //Apply the style which override for all app windows
    @available(iOS 13.0, *)
    func override(_ userInterfaceStyle: UIUserInterfaceStyle) {
        if supportsMultipleScenes {
            for connectedScene in connectedScenes {
                if let scene = connectedScene as? UIWindowScene {
                    for window in scene.windows {
                        window.overrideUserInterfaceStyle = userInterfaceStyle
                    }
                }
            }
        }
        else {
            for window in windows {
                window.overrideUserInterfaceStyle = userInterfaceStyle
            }
        }
    }
    
    func makeCall(to number: String, completion block: ((Bool) -> Void)? = nil) {
        if let url = URL(string: "tel://\(number)") {
            open(url, options: [:], completionHandler: block)
        }
    }
}
