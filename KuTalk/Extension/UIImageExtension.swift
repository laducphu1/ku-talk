//
//  UIImageExtension.swift
//  Torque Bot
//
//  Created by Nguyen Chi Dung on 11/14/19.
//  Copyright © 2019 Torque. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import SVProgressHUD
import FirebaseStorage

extension UIImage {
    var uncompressedPNGData: Data      { return pngData()!        }
    var highestQualityJPEGNSData: Data { return jpegData(compressionQuality: 1.0)!  }
    var highQualityJPEGNSData: Data    { return jpegData(compressionQuality: 0.75)! }
    var mediumQualityJPEGNSData: Data  { return jpegData(compressionQuality: 0.5)!  }
    var lowQualityJPEGNSData: Data     { return jpegData(compressionQuality: 0.25)! }
    var lowestQualityJPEGNSData:Data   { return jpegData(compressionQuality: 0.0)!  }
}

extension UIImage {
    
    func uploadImage(completionHandler: @escaping (_ avatarURL: String, _ error: Error?) -> ()) {
        SVProgressHUD.show()
        let imageName = UUID().uuidString
        let storage = Storage.storage().reference().child("message").child(imageName)
        if let uploadData = (self).jpegData(compressionQuality: 0.7) {
            storage.putData(uploadData, metadata: nil, completion: { (meta, error) in
                if error == nil {
                    storage.downloadURL(completion: { (urlImage, error) in
                        SVProgressHUD.dismiss()
                        if urlImage != nil {
                            completionHandler((urlImage?.absoluteString)!, nil)
                        } else {
                            completionHandler("", error)
                        }
                    })
                } else {
                    SVProgressHUD.dismiss()
                    completionHandler("", error)
                }
            })
        }
    }
    
    class func imageWithImage(image: UIImage) -> UIImage {
        let bubbleImage = image.resizableImage(withCapInsets: UIEdgeInsets(top: image.size.height/2, left: image.size.width/2, bottom: image.size.height/2, right: image.size.width/2), resizingMode: .stretch)
        return bubbleImage
    }
    
    
}
