//
//  CALayer.swift
//  Torque Bot
//
//  Created by Nguyen Chi Dung on 11/16/19.
//  Copyright © 2019 Torque. All rights reserved.
//

import UIKit

extension CALayer {
    
    func gradientLayerWithColor(beginColor: UIColor, beginAlpha: CGFloat, endColor: UIColor, endAlpha:CGFloat, bounds: CGRect) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer(layer: self)
               gradient.frame = bounds
        
        gradient.colors = [beginColor.withAlphaComponent(beginAlpha).cgColor, [endColor.withAlphaComponent(endAlpha).cgColor]]
        return gradient
    }
    
    func applySketchShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0) {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}
