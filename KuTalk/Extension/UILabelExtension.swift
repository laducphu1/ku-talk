//
//  UILabelExtension.swift
//  Torque Bot
//
//  Created by Nguyen Chi Dung on 12/12/19.
//  Copyright © 2019 Torque. All rights reserved.
//

import UIKit

extension UILabel {
    @IBInspectable var localize: String? {
        get { return nil }
        set(key) {
            text = key//?.localized()
        }
    }
    func indexOfAttributedTextCharacterAtPoint(point: CGPoint) -> Int {
        assert(self.attributedText != nil, "This method is developed for attributed string")
        let textStorage = NSTextStorage(attributedString: self.attributedText!)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: self.frame.size)
        textContainer.lineFragmentPadding = 0
        textContainer.maximumNumberOfLines = self.numberOfLines
        textContainer.lineBreakMode = self.lineBreakMode
        layoutManager.addTextContainer(textContainer)

        let index = layoutManager.characterIndex(for: point, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return index
    }
}

class PaddingLabel: UILabel {

   @IBInspectable var topInset: CGFloat = 5.0
   @IBInspectable var bottomInset: CGFloat = 5.0
   @IBInspectable var leftInset: CGFloat = 5.0
   @IBInspectable var rightInset: CGFloat = 5.0

   override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
   }

   override var intrinsicContentSize: CGSize {
      get {
         var contentSize = super.intrinsicContentSize
         contentSize.height += topInset + bottomInset
         contentSize.width += leftInset + rightInset
         return contentSize
      }
   }
}

extension UIButton {
    @IBInspectable var localize: String? {
        get { return nil }
        set(key) {
            setTitle(key, for: .normal) // ?.localized()
        }
    }
}

import SVProgressHUD

class CopyLabel: UILabel {
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        isUserInteractionEnabled = true
        addGestureRecognizer(tap)
    }
    
    @objc private func tapFunction(sender: UITapGestureRecognizer) {
        guard let text = self.text, text.count > 0, text != "--" else {return}
        UIPasteboard.general.string = text
        SVProgressHUD.showSuccess(withStatus: "\("Copy:") \(text)")
        SVProgressHUD.dismiss(withDelay: 1)
    } // .localized()
}


class EmojiTextField: UITextView {

   // required for iOS 13
//   override var textInputContextIdentifier: String? { "" } // return non-nil to show the Emoji keyboard ¯\_(ツ)_/¯

//    override var textInputMode: UITextInputMode? {
//        for mode in UITextInputMode.activeInputModes {
//            if mode.primaryLanguage == "emoji" {
//                return mode
//            }
//        }
//        return nil
//    }
    var languageCode:String?{
            didSet{
                if self.isFirstResponder{
                    self.resignFirstResponder();
                    self.becomeFirstResponder();
                }
            }
        }

        override var textInputMode: UITextInputMode?{
            if let language_code = self.languageCode{
                for keyboard in UITextInputMode.activeInputModes{
                    if let language = keyboard.primaryLanguage{
                        let locale = Locale.init(identifier: language);
                        if locale.languageCode == language_code{
                            return keyboard;
                        }
                    }
                }
            }
            return super.textInputMode;
        }
}
