//
//  NavigationItemExtension.swift
//  Torque Bot
//
//  Created by Nguyen Chi Dung on 12/6/19.
//  Copyright © 2019 Torque. All rights reserved.
//

import UIKit

extension UINavigationItem {
    func setLeftTitle(_ title: String) {
        let label = UILabel()
        label.textColor = .white
        label.text = title
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        leftBarButtonItem = UIBarButtonItem.init(customView: label)
    }
}
