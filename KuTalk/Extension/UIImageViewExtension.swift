//
//  UIImageViewExtension.swift
//  Torque Bot
//
//  Created by Nguyen Chi Dung on 11/14/19.
//  Copyright © 2019 Torque. All rights reserved.
//

import Foundation
import Kingfisher

extension UIImageView {
    
    func setImageUrl(_ imageUrl: URL?, placeholder: UIImage?, animated: Bool) {
        let placeholder = #imageLiteral(resourceName: "img_placeholder")
        var options = KingfisherOptionsInfo()
        if animated {
            options.append(.transition(.fade(0.4)))
        }
        self.kf.setImage(with: imageUrl,
                                   placeholder: placeholder,
                                   options: options)
    }
    class func imageWithImage(image: UIImage) -> UIImage {
        let bubbleImage = image.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 17, bottom: 17, right: 17), resizingMode: .stretch)
        return bubbleImage
    }
}


import Kingfisher
import ImageSlideshow

/// Input Source to image using Kingfisher
public class KingfisherSourceCustom: NSObject, InputSource {
    /// url to load
    public var image: UIImage
    var imageTitle: String?


    /// placeholder used before image is loaded
    public var placeholder: UIImage?
    
    /// options for displaying, ie. [.transition(.fade(0.2))]
    public var options: KingfisherOptionsInfo?

    /// Initializes a new source with a URL
    /// - parameter url: a url to be loaded
    /// - parameter placeholder: a placeholder used before image is loaded
    /// - parameter options: options for displaying
    public init(image: UIImage, imageTitle: String?, placeholder: UIImage? = nil, options: KingfisherOptionsInfo? = nil) {
        self.image = image
        self.placeholder = placeholder
        self.options = options
        self.imageTitle = imageTitle
        super.init()
    }

    /// Initializes a new source with a URL string
    /// - parameter urlString: a string url to load
    /// - parameter placeholder: a placeholder used before image is loaded
    /// - parameter options: options for displaying
//    public init?(urlString: String, imageTitle: String?, placeholder: UIImage? = nil, options: KingfisherOptionsInfo? = nil) {
//        if let validUrl = URL(string: urlString) {
//            self.url = validUrl
//            self.placeholder = placeholder
//            self.options = options
//            self.imageTitle = imageTitle
//            super.init()
//        } else {
//            return nil
//        }
//    }
    
    /// Load an image to an UIImageView
    ///
    /// - Parameters:
    ///   - imageView: UIImageView that receives the loaded image
    ///   - callback: Completion callback with an optional image
    @objc
    public func load(to imageView: UIImageView, with callback: @escaping (UIImage?) -> Void) {
        imageView.addSubview(imageTitleLabel())
        callback(self.image)
    }
    
    private func imageTitleLabel() -> UILabel {

        let label = UILabel.init(frame: CGRect.init(x: UIScreen.main.bounds.size.width/2 + 3, y: 80, width: 0, height: 0))
        label.font = UIFont(name: "Quicksand-Medium", size: 30)
        label.textAlignment = .left
        label.textColor = .white
        label.text = imageTitle
        label.sizeToFit()
        return label
    }
    
    /// Cancel an image download task
    ///
    /// - Parameter imageView: UIImage view with the download task that should be canceled
    public func cancelLoad(on imageView: UIImageView) {
        imageView.kf.cancelDownloadTask()
    }
}
