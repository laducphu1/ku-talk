//
//  StringExtension.swift
//  Torque Bot
//
//  Created by Nguyen Chi Dung on 11/14/19.
//  Copyright © 2019 Torque. All rights reserved.
//

import Foundation
import UIKit
//import libPhoneNumber_iOS

extension String {
    
    func getRangeOfSubString(subString: String) -> NSRange {
        guard let sampleLinkRange = range(of: subString) else {
            return NSMakeRange(0,0)
        }
        let startPos = distance(from: startIndex, to: sampleLinkRange.lowerBound)
        let endPos = distance(from: startIndex, to: sampleLinkRange.upperBound)
        let linkRange = NSMakeRange(startPos, endPos - startPos)
        return linkRange
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func isValidEmail()-> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidName() -> Bool {
        let characterSet = CharacterSet(charactersIn: "!\"#$%&'()*+,-./:;<=>?@\\[\\\\\\]^_`{|}~")
        if rangeOfCharacter(from: characterSet) != nil {
            return false
        }
        return true;
    }
    
//    func isValidPhone() -> Bool {
//        let phoneUtil = NBPhoneNumberUtil()
//        do {
//            let phoneNumber: NBPhoneNumber = try phoneUtil.parse(self, defaultRegion: "VN")
//            return phoneUtil.isValidNumber(phoneNumber)
//        }
//        catch  _ as NSError {
//            return false
//        }
//    }
    
    func isValidPassword() -> Bool {
        //Minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character:
        let passRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$"
        let passTest = NSPredicate(format: "SELF MATCHES %@", passRegEx)
        return passTest.evaluate(with: self)
    }
    
    func trim()->String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func toNSError() -> NSError {
        let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : self])
        return error
    }
    
    func toFloat() -> CGFloat {
        let numberFormatter = NumberFormatter.init()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSeparator = ","
//        numberFormatter.maximumFractionDigits = 10
        numberFormatter.minimumFractionDigits = 10
        numberFormatter.decimalSeparator = "."
        return CGFloat(numberFormatter.number(from: self)?.doubleValue ?? 0)
    }
    
    func toNumberString() -> String {
        let floatValue = Float(self) ?? 0
        if floatValue == 0 {
            return "0"
        }
        let numberFormatter = NumberFormatter.init()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSeparator = ","
        numberFormatter.decimalSeparator = "."
        numberFormatter.maximumFractionDigits = 2
        let numberString = numberFormatter.string(from: NSNumber(value: floatValue)) ?? ""
        return numberString
    }
    
    func toPercenString() -> String {
        
        return "\(toFloat().toString(roundTo: 8, 0)) %"
    }
    
    
    enum TruncationPosition {
        case head
        case middle
        case tail
    }

    func truncated(limit: Int, position: TruncationPosition = .tail, leader: String = "...") -> String {
        var limit = limit
        if K.Device.IS_IPHONE_5 {
            limit = limit - 10
        }
        if K.Device.IS_IPHONE_6 {
            limit = limit - 5
        }
        guard self.count > limit else { return self }
        switch position {
        case .head:
            return leader + self.suffix(limit)
        case .middle:
            let headCharactersCount = Int(ceil(Float(limit - leader.count) / 2.0))

            let tailCharactersCount = Int(floor(Float(limit - leader.count) / 2.0))
            
            return "\(self.prefix(headCharactersCount))\(leader)\(self.suffix(tailCharactersCount))"
        case .tail:
            return self.prefix(limit) + leader
        }
    }
    
    mutating func standardizedNumber(regex: String) {
            do {
                let regex = try NSRegularExpression(pattern: regex, options: NSRegularExpression.Options.caseInsensitive)
    //            let regex = try NSRegularExpression(pattern: "(?:(\.\d*?[1-9]+)|\.)0*$", options: NSRegularExpression.Options.caseInsensitive)
                let range = NSMakeRange(0, self.count)
                self = regex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: "")
            } catch {
                return
            }
        }
    
    var htmlToAttributedString: NSAttributedString? {
//        var string = replacingOccurrences(of: "\"", with: "'")
        let string = replacingOccurrences(of: "\\", with: "")
        guard let data = string.data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    var htmlToAttributedString2: NSAttributedString? {
            guard let data = data(using: .utf8) else { return NSAttributedString() }
            do {
                return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            } catch {
                return NSAttributedString()
            }
        }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func isValidBTCAddress() -> Bool {
        let fullAddress = self.components(separatedBy: ":")
        if fullAddress.count == 1 {
            let pattern = "^(bc(0([ac-hj-np-z02-9]{39}|[ac-hj-np-z02-9]{59})|1[ac-hj-np-z02-9]{8,87})|[13][a-km-zA-HJ-NP-Z1-9]{25,35})$"
            let r = fullAddress[0].startIndex..<fullAddress[0].endIndex
            let r2 = fullAddress[0].range(of: pattern, options: .regularExpression)
            return r == r2
        } else {
            return false
        }
    }
    
    func isValidLTCAddress() -> Bool {
        let fullAddress = self.components(separatedBy: ":")
        if fullAddress.count == 1 {
            let pattern = "^([LM3Q2mn][a-km-zA-HJ-NP-Z1-9]{26,34}|ltc1[a-zA-Z0-9]{26,45})$"
            let r = fullAddress[0].startIndex..<fullAddress[0].endIndex
            let r2 = fullAddress[0].range(of: pattern, options: .regularExpression)
            return r == r2
        } else {
            return false
        }
    }
    func isValidETHAndUSDTAddress() -> Bool {
        let fullAddress = self.components(separatedBy: ":")
        if fullAddress.count == 1 {
            let pattern = "^0x[a-fA-F0-9]{40}$"
            let r = fullAddress[0].startIndex..<fullAddress[0].endIndex
            let r2 = fullAddress[0].range(of: pattern, options: .regularExpression)
            return r == r2
        } else {
            return false
        }
    }
}


extension Date {
      var convertedDate:Date {
          let dateFormatter = DateFormatter();
          let dateFormat = "dd MMM yyyy";
          dateFormatter.dateFormat = dateFormat;
          let formattedDate = dateFormatter.string(from: self);

          dateFormatter.locale = NSLocale.current;
          dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00");

          dateFormatter.dateFormat = dateFormat as String;
          let sourceDate = dateFormatter.date(from: formattedDate as String);

          return sourceDate!;
      }
}

extension String {

    var length: Int {
        return count
    }

    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }

    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }

    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }

    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
}


extension UnicodeScalar {
    /// Note: This method is part of Swift 5, so you can omit this.
    /// See: https://developer.apple.com/documentation/swift/unicode/scalar
    var isEmoji: Bool {
        switch value {
        case 0x1F600...0x1F64F, // Emoticons
             0x1F300...0x1F5FF, // Misc Symbols and Pictographs
             0x1F680...0x1F6FF, // Transport and Map
             0x1F1E6...0x1F1FF, // Regional country flags
             0x2600...0x26FF, // Misc symbols
             0x2700...0x27BF, // Dingbats
             0xE0020...0xE007F, // Tags
             0xFE00...0xFE0F, // Variation Selectors
             0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
             0x1F018...0x1F270, // Various asian characters
             0x238C...0x2454, // Misc items
             0x20D0...0x20FF: // Combining Diacritical Marks for Symbols
            return true

        default: return false
        }
    }

    var isZeroWidthJoiner: Bool {
        return value == 8205
    }
}

extension String {
    // Not needed anymore in swift 4.2 and later, using `.count` will give you the correct result
    var glyphCount: Int {
        let richText = NSAttributedString(string: self)
        let line = CTLineCreateWithAttributedString(richText)
        return CTLineGetGlyphCount(line)
    }

    var isSingleEmoji: Bool {
        return glyphCount == 1 && containsEmoji
    }

    var containsEmoji: Bool {
        return unicodeScalars.contains { $0.isEmoji }
    }

    var containsOnlyEmoji: Bool {
        return !isEmpty
            && !unicodeScalars.contains(where: {
                !$0.isEmoji && !$0.isZeroWidthJoiner
            })
    }

    // The next tricks are mostly to demonstrate how tricky it can be to determine emoji's
    // If anyone has suggestions how to improve this, please let me know
    var emojiString: String {
        return emojiScalars.map { String($0) }.reduce("", +)
    }

    var emojis: [String] {
        var scalars: [[UnicodeScalar]] = []
        var currentScalarSet: [UnicodeScalar] = []
        var previousScalar: UnicodeScalar?

        for scalar in emojiScalars {
            if let prev = previousScalar, !prev.isZeroWidthJoiner, !scalar.isZeroWidthJoiner {
                scalars.append(currentScalarSet)
                currentScalarSet = []
            }
            currentScalarSet.append(scalar)

            previousScalar = scalar
        }

        scalars.append(currentScalarSet)

        return scalars.map { $0.map { String($0) }.reduce("", +) }
    }

    fileprivate var emojiScalars: [UnicodeScalar] {
        var chars: [UnicodeScalar] = []
        var previous: UnicodeScalar?
        for cur in unicodeScalars {
            if let previous = previous, previous.isZeroWidthJoiner, cur.isEmoji {
                chars.append(previous)
                chars.append(cur)

            } else if cur.isEmoji {
                chars.append(cur)
            }

            previous = cur
        }

        return chars
    }
}
