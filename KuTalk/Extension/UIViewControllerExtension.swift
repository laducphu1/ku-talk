//
//  UIViewControllerExtension.swift
//  Torque Bot
//
//  Created by Nguyen Chi Dung on 11/18/19.
//  Copyright © 2019 Torque. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    static var topViewController: UIViewController {
        return TopViewController()
    }
    
    /* Get current top screen */
    
    static func TopViewController( of viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController ) -> UIViewController {
        if let viewController = viewController as? UIPageViewController {
            return TopViewController(of: viewController.viewControllers?.first)
        }
        
        if let viewController = viewController as? UINavigationController {
            return TopViewController(of: viewController.visibleViewController)
        }
        
        if let viewController = viewController as? UITabBarController {
            if let viewController = viewController.selectedViewController {
                return TopViewController(of: viewController)
            }
        }
        
        if let viewController = viewController?.presentedViewController {
            return TopViewController(of: viewController)
        }
        
        return viewController ?? UIViewController()
    }
    
    func showAlert(title: String? = "Thông báo", message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}
