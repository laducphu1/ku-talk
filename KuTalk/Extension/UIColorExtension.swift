//
//  UIColorExtension.swift
//
//
//  Created by Nguyen Chi Dung on 11/14/19.
//  Copyright © 2019 Torque. All rights reserved.
//

import Foundation
import UIKit

enum AssetsColor : String {
    case appRedColor
    case background
    case appShadowColor
    case appBackground
    case blackColor
    case blackTextColor
    case subTitleColor
    case titleColor
    case lightBorderColor
    case whiteBackground
    case viewAll
    case seperator
    case buttonBackground
    case messageReceiver
    case messageSender
    
    case tableViewBackground
    case sendText
    case receiverText
}

extension UIColor {
    
    convenience init(hex: Int) {
      let components = (
        R: CGFloat((hex >> 16) & 0xff) / 255,
        G: CGFloat((hex >> 08) & 0xff) / 255,
        B: CGFloat((hex >> 00) & 0xff) / 255
      )
        
      self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
    
    static func appColor() -> UIColor {
        return UIColor(named: AssetsColor.blackColor.rawValue)!
    }
    
    static func appRedColor() -> UIColor {
        return UIColor(named: AssetsColor.appRedColor.rawValue)!
    }
    
    static func colorPalette() -> [UIColor] {
        let path = Bundle.main.path(forResource: "colorPalette", ofType: "plist")
        guard let pListArray = NSArray(contentsOfFile: path!) else {return []}
        var colors = [UIColor]()
        for color in pListArray {
            if let hex = color as? String {
                colors.append(UIColor(hex: hex))
            }
        }
        return colors
    }

    static func appColor(_ name: AssetsColor) -> UIColor {
        switch name {
        case .messageSender:
            return UIColor.getColor(dark: UIColor(hex: "44BEC7"), light: UIColor(hex: "44BEC7"))
        case .messageReceiver:
            return UIColor.getColor(dark: UIColor(hex: "3F4041"), light: UIColor(hex: "EEEEEE"))
            
        case .sendText:
            return UIColor.getColor(dark: UIColor(hex: "FFFFFF"), light: UIColor(hex: "000000"))
        case .receiverText:
            return UIColor.getColor(dark: UIColor(hex: "FFFFFF"), light: UIColor(hex: "000000"))
        case .tableViewBackground:
            return UIColor.getColor(dark: UIColor(hex: "000000"), light: UIColor(hex: "F4F5F6"))
        case .subTitleColor:
            return UIColor.getColor(dark: UIColor(hex: "000000").withAlphaComponent(0.75), light: UIColor(hex: "FFFFFF"))

        default:
            return UIColor(named: name.rawValue)!

        }
    }
    
    static func getColor(dark: UIColor, light: UIColor) -> UIColor{
        if AppDataSingleton.shared.isDarkModeEnable {
            return dark
        }
        return light
    }
}


extension UIColor {
    var HexString: String {
        var red:   CGFloat = 0
        var green: CGFloat = 0
        var blue:  CGFloat = 0
        var alpha: CGFloat = 0

        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)

        return String(format: "%02X%02X%02X",
                    Int(red * 0xff),
                    Int(green * 0xff),
                    Int(blue * 0xff)
        )
    }

    convenience
    init( hex: String ) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0

        var rgbValue: UInt64 = 0

        scanner.scanHexInt64(&rgbValue)

        let red   = (rgbValue & 0xff0000) >> 16
        let green = (rgbValue & 0xff00) >> 8
        let blue  = rgbValue & 0xff

        self.init(red: CGFloat(red) / 0xff,
                    green: CGFloat(green) / 0xff,
                    blue: CGFloat(blue) / 0xff, alpha: 1
        )
    }
    
    convenience init(light: UIColor, dark: UIColor) {
        if #available(iOS 13.0, tvOS 13.0, *) {
            self.init { traitCollection in
                if traitCollection.userInterfaceStyle == .dark {
                    return dark
                }
                return light
            }
        }
        else {
            self.init(cgColor: light.cgColor)
        }
    }
}
