//
//  NumberExtension.swift
//  Torque Bot
//
//  Created by Nguyen Chi Dung on 11/20/19.
//  Copyright © 2019 Torque. All rights reserved.
//

import UIKit

extension CGFloat {
    
    func toTorqueCurrencyString() -> String {
        return torqueCurrencyString() + " TORQ"
    }
    
    func toLTCCurrencyString() -> String {
        return torqueCurrencyString() + " LTC"
    }
    
    func toBTCCurrencyString() -> String {
        return torqueCurrencyString() + " BTC"
    }
    
    func toETHCurrencyString() -> String {
        return torqueCurrencyString() + " ETH"
    }
    
    func toUSDTCurrencyString() -> String {
        return torqueCurrencyString() + " USDT"
    }
    
    func toCurrencyString() -> String {
        return "$" + currencyString()
    }
    
    func toRateString() -> String {
        return "≈ $" + currencyString()
    }
    
    func currencyString() -> String {
        return toString(roundTo: 2)
    }
    
    func downlinePercentString() -> String {
        let numberFormatter = NumberFormatter.init()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSeparator = ","
        numberFormatter.decimalSeparator = "."
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = 2
        let stringValue = numberFormatter.string(from: NSNumber(value: Double(self))) ?? "0"
        return stringValue
    }
    
    func toString(roundTo places: Int, _ minimumFract: Int? = nil) -> String {
        if self == 0 {
            return "0"
        }
        let numberFormatter = NumberFormatter.init()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSeparator = ","
        numberFormatter.decimalSeparator = "."
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = 10
        let stringValue = numberFormatter.string(from: NSNumber(value: Double(self))) ?? "0"
        let ar = stringValue.split(separator: ".")
        if ar.count > 1 {
            var string = String(ar.last?.prefix(places) ?? "")
            if string.toFloat() == 0 {
                 return String(ar.first ?? "")
            }
            while string.count > 0, string.last == "0" {
                string = String(string.prefix(string.count - 1))
            }
            return "\(ar.first ?? "").\(string)"
        }
        return String(ar.first ?? "")
    }
    
    func toString() -> String {
        return toString(roundTo: 8)
    }
    
    func torqueCurrencyString() -> String {
        return toString(roundTo: 8)
    }

}
