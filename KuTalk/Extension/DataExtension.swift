//
//  DataExtension.swift
//  KuTalk
//
//  Created by flydino on 1/27/21.
//

import Foundation

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
