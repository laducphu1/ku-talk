//
//  UIStoryBoard.swift
//  Torque Bot
//
//  Created by Nguyen Chi Dung on 11/17/19.
//  Copyright © 2019 Torque. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    class func legacyStoryboard()->UIStoryboard {
        return UIStoryboard.init(name: K.Storyboard.Name.Legacy, bundle: nil)
    }
    
    class func mainStoryboard()->UIStoryboard {
        return UIStoryboard.init(name: K.Storyboard.Name.Main, bundle: nil)
    }
    
    class func authenticationStoryboard()->UIStoryboard {
        return UIStoryboard.init(name: K.Storyboard.Name.Authentication, bundle: nil)
    }
    
    class func walletStoryboard()->UIStoryboard {
        return UIStoryboard.init(name: K.Storyboard.Name.Wallet, bundle: nil)
    }
    
    class func profileStoryboard()->UIStoryboard {
        return UIStoryboard.init(name: K.Storyboard.Name.Profile, bundle: nil)
    }
    
    class func profitStoryboard()->UIStoryboard {
        return UIStoryboard.init(name: K.Storyboard.Name.Profit, bundle: nil)
    }
    
    class func depositStoryboard()->UIStoryboard {
        return UIStoryboard.init(name: K.Storyboard.Name.Deposit, bundle: nil)
    }
    
    class func affiliateStoryboard()->UIStoryboard {
        return UIStoryboard.init(name: K.Storyboard.Name.Affiliate, bundle: nil)
    }
    
    class func charityStoryboard()->UIStoryboard {
        return UIStoryboard.init(name: K.Storyboard.Name.Charity, bundle: nil)
    }

}
