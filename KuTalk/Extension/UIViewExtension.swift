//
//  UIView+Additions.swift
//  Torque Bot
//
//  Created by Nguyen Chi Dung on 11/14/19.
//  Copyright © 2019 Torque. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    static func fromNib( ) -> Self {
        func impl<Type:UIView>( type: Type.Type ) -> Type {
            return Bundle.main.loadNibNamed(String(describing: type), owner: nil, options: nil)!.first as! Type
        }
        
        return impl(type: self)
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    var isShow: Bool {
        set { isHidden = !newValue }
        get { return !isHidden }
    }
    
    func addShadowView(withColor color: UIColor) {
        self.layer.applySketchShadow(color: color, alpha: 0.11, x: 0, y: 20, blur: 40, spread: 7)

    }
    
    func makeRounded(radius: CGFloat, corners: UIRectCorner, frame: CGRect? = nil, borderWidth: CGFloat = 0, borderColor: UIColor? = nil) {
        if let frame = frame {
            self.bounds = frame
        }
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: corners,
                                     cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
                
        if borderWidth > 0 {
            let borderLayer = CAShapeLayer()
            borderLayer.path = maskPath1.cgPath
            borderLayer.lineWidth = borderWidth
            borderLayer.strokeColor = borderColor?.cgColor ?? UIColor.appColor(.lightBorderColor).cgColor
            borderLayer.fillColor = UIColor.clear.cgColor
            
            layer.addSublayer(borderLayer)
        }
        
        layer.mask = maskLayer1
    }
    
    func addShadow(ofColor color: UIColor = UIColor(red: 0.07, green: 0.47, blue: 0.57, alpha: 1.0), radius: CGFloat = 3, offset: CGSize = .zero, opacity: Float = 0.5) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        layer.masksToBounds = false
    }
}

//extension CALayer {
//  func applySketchShadow(
//    color: UIColor = .black,
//    alpha: Float = 0.11,
//    x: CGFloat = 0,
//    y: CGFloat = 20,
//    blur: CGFloat = 40,
//    spread: CGFloat = 7)
//  {
//    masksToBounds = false
//    shadowColor = color.cgColor
//    shadowOpacity = alpha
//    shadowOffset = CGSize(width: x, height: y)
//    shadowRadius = blur / 2.0
//    if spread == 0 {
//      shadowPath = nil
//    } else {
//      let dx = -spread
//      let rect = bounds.insetBy(dx: dx, dy: dx)
//      shadowPath = UIBezierPath(rect: rect).cgPath
//    }
//  }
//}

extension UIView {
    func addShadow(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat, cornerRadius: CGFloat, corners: UIRectCorner, fillColor: UIColor = .clear) {
        
        let shadowLayer = CAShapeLayer()
        let size = CGSize(width: cornerRadius, height: cornerRadius)
        let cgPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size).cgPath //1
        shadowLayer.path = cgPath //2
        shadowLayer.fillColor = fillColor.cgColor //3
        shadowLayer.shadowColor = shadowColor.cgColor //4
        shadowLayer.shadowPath = cgPath
        shadowLayer.shadowOffset = offSet //5
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = shadowRadius
        self.layer.addSublayer(shadowLayer)
    }
    
    open func generateOuterShadow(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat, cornerRadius: CGFloat) {
        
        layer.cornerRadius = cornerRadius
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = opacity
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = offSet
        self.clipsToBounds = true
        
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = layer.cornerRadius
        view.layer.shadowRadius = layer.shadowRadius
        view.layer.shadowOpacity = layer.shadowOpacity
        view.layer.shadowColor = layer.shadowColor
        view.layer.shadowOffset = layer.shadowOffset
        view.clipsToBounds = false
        view.backgroundColor = .white
        superview?.insertSubview(view, belowSubview: self)
        let constraints = [
            NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0),
        ]
     
        superview?.addConstraints(constraints)
    }
   
    
}
extension UIView {

    /**
     Removes all constrains for this view
     */
    func removeConstraints() {

        let constraints = self.superview?.constraints.filter{
            $0.firstItem as? UIView == self || $0.secondItem as? UIView == self
        } ?? []

        self.superview?.removeConstraints(constraints)
        self.removeConstraints(self.constraints)
    }
}
