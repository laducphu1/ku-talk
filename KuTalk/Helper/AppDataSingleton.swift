//
//  AppDataSingleton.swift
//  KuTalk
//
//  Created by Fu on 1/26/21.
//

import UIKit
import Firebase

enum Defaults {
    static func set(_ object: Any, forKey defaultName: String) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.set(object, forKey:defaultName)
        defaults.synchronize()
        
    }
    static func object(forKey key: String) -> AnyObject! {
        let defaults: UserDefaults = UserDefaults.standard
        return defaults.object(forKey: key) as AnyObject?
    }
}

class AppDataSingleton: NSObject {
    static let shared = AppDataSingleton()
    private var savedAccounts = [[String:String]]()
    
    var liveStreamingStatus = ""
    var securityTextEntryIsEnable = false
    //var legacy: Legacy?
    var isMantenance = false
    var fcmTokens = [String]()
    //var tmall = News()
    
    var isDarkModeEnable: Bool! {
        get {
            return  UserDefaults.standard.bool(forKey: "IsDarkModeEnable")
        }
        set(check) {
            UserDefaults.standard.set(check, forKey: "IsDarkModeEnable")
        }
    }

    var ref: DatabaseReference!
    var storageRef: StorageReference!
    
    lazy var dateFormatter: DateFormatter = {
        let readDateFormat = DateFormatter()
        readDateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        readDateFormat.timeZone = TimeZone.init(identifier: "UTC")
        readDateFormat.locale = Locale(identifier: "en_US_POSIX")
        return readDateFormat
    }()
    
    lazy var simpleDateFormatter: DateFormatter = {
        let readDateFormat = DateFormatter()
        readDateFormat.dateFormat = "yyyy-MM-dd"
        readDateFormat.timeZone = TimeZone.init(identifier: "UTC")
        readDateFormat.locale = Locale(identifier: "en_US_POSIX")
        return readDateFormat
    }()
    
    var displayDateFormatter: DateFormatter {
        get {
            let readDateFormat = DateFormatter()
            readDateFormat.dateFormat = "MMM dd, yyyy"
            readDateFormat.timeZone = TimeZone.init(identifier: "UTC")
            readDateFormat.locale = Locale(identifier: "LocaleIdentifier") //.localized())
            return readDateFormat
        }
    }
    
    var currentUser: User? {
        get {
            let currentUserDict = Defaults.object(forKey: "CurrentUser") as? [String : Any]
            if currentUserDict == nil {
                return nil
            }
            if (currentUserDict?.count) != 0 {
                return User(JSON: currentUserDict!)
            }
            return nil
        }
        
        set(currentUser) {
            if currentUser != nil {
                let currentUserDict = currentUser?.toDictionary()
                Defaults.set(currentUserDict!, forKey: "CurrentUser")
            } else {
                Defaults.set([:], forKey: "CurrentUser")
            }
        }
    }
    
    var pushNotificationID: String?
    
    override init() {
        super.init()
        ref = Database.database().reference()
        storageRef = Storage.storage().reference()
    }
    
    func updateInterfaceStyle() {
        if #available(iOS 13.0, *) {
            let style = AppDataSingleton.shared.isDarkModeEnable ? UIUserInterfaceStyle.dark : UIUserInterfaceStyle.light
            UIApplication.shared.override(style)
        }
    }
    
    func handlePhoneNumber(_ phone: String) -> String {
        var phoneHandle = phone.replacingOccurrences(of: " ", with: "")
        if phone.substring(toIndex: 1) == "0" {
            phoneHandle = phoneHandle.replacingCharacters(in: ...phoneHandle.startIndex, with: "+84")
        }
        return phoneHandle
    }
    
    func setInitialVC(isHome: Bool = true) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let storyboard = UIStoryboard(name: isHome ? "Main" : "Authentication", bundle: nil)
        if let vc = storyboard.instantiateInitialViewController() {
            if #available(iOS 13.0, *) {
                if let scene = UIApplication.shared.connectedScenes.first {
                    guard let windowScene = (scene as? UIWindowScene) else { return }
                    let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                    window.windowScene = windowScene
                    window.rootViewController = vc
                    window.makeKeyAndVisible()
                    appDelegate.window = window
                }
            } else {
                appDelegate.window?.rootViewController = vc
                appDelegate.window?.makeKeyAndVisible()
            }
        }
    }
    
    //    var accessToken: String? {
    //        get {
    //            return UserDefaults.standard.object(forKey: K.UserDefaults.AccessToken) as? String
    //        }
    //        set(accessToken) {
    //            UserDefaults.standard.set(accessToken, forKey: K.UserDefaults.AccessToken)
    //        }
    //    }
    
    //    var keepLogin: Bool? {
    //        get {
    //            return UserDefaults.standard.bool(forKey: K.UserDefaults.KeepLogin)
    //        }
    //        set(value) {
    //            UserDefaults.standard.set(value, forKey: K.UserDefaults.KeepLogin)
    //        }
    //    }
    
    func getSavedAccounts() -> [[String:String]] {
        if let savedAccount = UserDefaults.standard.object(forKey: K.UserDefaults.SavedAccounts) as? [[String:String]] {
            self.savedAccounts = savedAccount
        }
        
        return savedAccounts
    }
    
    func handleTime(with date: Date) -> String {
        if date.isInToday {
            return date.string(withFormat: "HH:mm")
        }
        if date.isInCurrentWeek {
            return date.string(withFormat: "EEE")
        }
        return date.string(withFormat: "dd-MM-yyyy")
    }
    
    func countUserNumbersRoom(with roomName: String, completion: @escaping (Int) -> ()) {
        ref.child("Rooms/\(roomName)/users").observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? [String: Any] {
                completion(dict.keys.count)
            } else {
                completion(0)
            }
        }
    }
}

