//
//  AppAppearance.swift
//  KuTalk
//
//  Created by mac on 1/26/21.
//

import UIKit

class AppAppearance: NSObject {
    static func config() {
        //        configTabbar()
        configNavigationBar()
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for: UIBarMetrics.default)
    }
    
    static func configNavigationBar() {
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.appColor(.blackTextColor)
        UINavigationBar.appearance().barTintColor = UIColor.appColor(.whiteBackground)
        let fontNavBar = UIFont.systemFont(ofSize: 20, weight: .medium)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: fontNavBar]
        
        let backIcon = UIImage.init(named: "ic_back_white")?.withRenderingMode(.alwaysTemplate)
        
        UINavigationBar.appearance().backIndicatorImage = backIcon
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backIcon
    }
    
    private static func configTabbar() {
        UITabBar.appearance().isTranslucent = false
        UITabBarItem.appearance().setTitleTextAttributes([.foregroundColor: UIColor.systemGray], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([.foregroundColor: UIColor.appColor(.blackColor)], for: .selected)
        
        UITabBar.appearance().shadowImage = .init()
        UITabBar.appearance().backgroundImage = .init()
        //UITabBar.appearance().tintColor = UIColor.systemBlue
        
//        if #available(iOS 13.0, *) {
//            UITabBar.appearance().standardAppearance.backgroundColor = .red
//        } else {
//            UITabBar.appearance().backgroundColor = UIColor.appColor(.whiteBackground)
//        }
    }
}
