//
//  Constants.swift
//  KuTalk
//
//  Created by Fu on 1/26/21.
//

import UIKit

struct K {
    struct App {
        static let AppStoreId = ""
        static let MinVersion = 22
    }
    
    struct CellIdentifier {
       
    }
    
    struct API {
        
    }

    struct Storyboard {
        struct Name {
            static let Main = "Main"
            static let Home = "Home"
            static let Wallet = "Wallet"
            static let Profit = "Profit"
            static let Affiliate = "Affiliate"
            static let Account = "Account"
            static let Authentication = "Authentication"
            static let Profile = "Profile"
            static let Deposit = "Deposit"
            static let Legacy = "Legacy"
            static let Charity = "Charity"
        }
        struct SegueIdentifier {
            static let TermsAndConditions = "TermsAndConditionsSegueIdentifier"
            static let PrivacyPolicy = "PrivacyPolicySegueIdentifier"
        }
        struct VCIdentifier {
            static let Profile = "ProfileVC"
            static let Home = "HomeVC"

        }
    }
    
    
    struct Model {
        
        static let Id = "user_id"
        static let FirstName = "first_name"
        static let FullName = "full_name"
        static let Email = "email"
        static let Phone = "phone"
        static let LastName = "last_name"
        static let Code = "code"
        static let Dob = "dob"
        static let Nation = "nation"
        static let Password = "password"
    }
    
    struct Device {
        static let IS_IPHONE_4 = (UIScreen.main.bounds.size.height == 480.0)
        static let IS_IPHONE_5 = (UIScreen.main.bounds.size.height == 568.0)
        static let IS_IPHONE_6 = (UIScreen.main.bounds.size.height == 667.0)
        static let IS_IPHONE_6_PLUS = (UIScreen.main.bounds.size.height == 736.0)
        static let IS_IPHONE_X = (UIScreen.main.bounds.size.height == 812.0)
    }
    struct UserDefaults {
        static let LoggedInUser = "LoggedInUser"
        static let SavedAccounts = "SavedAccounts"
    }
    
    struct Resource {
        
    }
    
    struct Font {
        static let Bold = ""
        static let SemiBold = ""
        static let Medium = ""
        static let Regular = ""
        static let Light = ""
    }
    struct Color {
        static let LuxorGold = #colorLiteral(red: 0.6666666667, green: 0.4745098039, blue: 0.2588235294, alpha: 1)
    }
    struct FontIcon {
        static let Ionicons = "Ionicons"
        static let Material = "Material"
    }
    struct Value {
        
    }
    
    struct Language {
       
    }
    
    struct SupportContact {
        static let Email = "support@abc.com"
        static let Phone = "+123456"
    }
}

